﻿using System;
using Adic;
using Game.Scripts.Controllers.BattleController;
using Game.Scripts.Data.Actions;
using Game.Scripts.Enums;
using Game.Scripts.Types;
using System.Collections.Generic;
using System.Linq;
using Game.Scripts.Base;


//DON'T TOUCH IT, IN CASE  OF EMERGENCY USE  the "FAN" and "BALOON"!
namespace Game.Scripts.TMP
{
    public static class UnitParametersSwitcher
    {
        [Inject] private static IBattleData battleData;
        [Inject] private static IBattleCommand battleCommand;

        static UnitParametersSwitcher()
        {
            battleData = GameContextRoot.Container.Resolve<IBattleData>();
            battleCommand = GameContextRoot.Container.Resolve<IBattleCommand>();
        }

        public static bool ExecuteAction(CardActionSettings action, Unit target)
        {
            if(action.IsStun)target.StunUnit(action.RoundsCounter);
            if (action.AttackType == AttackType.Heal) return SetHeal(action, target);
            else
            {
                return SetDamage(action, target);
            }
        }

        public static bool SetHeal(CardActionSettings action, Unit target)
        {
            if (!SortedTargets(action, target).Contains(target)) return false;

            int value = Convert.ToInt32(Math.Round(battleData.CurrentUnit.HealPower * action.Multiplier));

            if (action.AttackSelection == AttackSelection.Single)
            {
                battleCommand.SetHeal(target, value);
                return true;
            }
            else if (action.AttackSelection == AttackSelection.Massive)
            {
                foreach (Unit sortedTarget in SortedTargets(action, target))
                {
                    battleCommand.SetHeal(sortedTarget, value);
                }

                return true;
            }
            return false;
        }

        public static bool SetDamage(CardActionSettings action, Unit target)
        {
            var temp = SortedTargets(action, target);
            if (!temp.Contains(target)) return false;

            if (action.AttackSelection == AttackSelection.Single)
            {
                battleCommand.SetDamage(target, ReturnDamageValue(action, target));
                return true;
            }

            if (action.AttackSelection == AttackSelection.Massive)
            {
                foreach (Unit targetUnit in SortedTargets(action, target))
                {
                    battleCommand.SetDamage(targetUnit, ReturnDamageValue(action, target));
                }

                return true;
            }
            return false;
        }
        /// <summary>
        /// Возвращает готовый список целей в зависимости от параметров
        /// направления атаки и к кому применяется действие
        /// </summary>
        /// <param name="action"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        private static List<Unit> SortedTargets(CardActionSettings action, Unit target)
        {
            return AttackDirectionList(target,
                TargetsList(target, action.ActionApplying),
                action.AttackDirection);
        }

        private static int ReturnDamageValue(CardActionSettings action, Unit target)
        {
            int value = Convert.ToInt32(Math.Round(TypeOfPower(action.AttackType) * action.Multiplier));
            int resistance = TypeOfResistance(action.DamageType, target);
            int result = value - resistance;
            if (result <= 0) return 0;
            return value;

        }

        private static int TypeOfPower(AttackType attackType)
        {
            switch (attackType)
            {
                case AttackType.Physical:
                    return battleData.CurrentUnit.PhysicalPower;
                case AttackType.Magic:
                    return battleData.CurrentUnit.MagicalPower;
                default:
                    return 0;
            }
        }

        private static int TypeOfResistance(DamageType damage, Unit target)
        {
            switch (damage)
            {
                case DamageType.Physical:

                    return target.PhysicalResist;

                case DamageType.Fire:

                    return target.FireResist;

                case DamageType.Poison:
                    return target.PoisonResist;
                case DamageType.Ice:
                default:
                    return 0;
            }
        }


        /// <summary>
        /// Возвращает список целей в зависимости к кому применимо действие
        /// </summary>
        /// <param name="target"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        private static List<Unit> TargetsList(Unit target, ActionApplying action)
        {
            switch (action)
            {
                case ActionApplying.Comrades:
                    {
                        return battleData.UnitsQueue.Where(x => x.UnitType == battleData.CurrentUnit.UnitType)
                            .Where(x => !x.IsDead).ToList();
                    }
                case ActionApplying.Enemies:
                    {
                        return battleData.UnitsQueue.Where(x => x.UnitType != battleData.CurrentUnit.UnitType)
                            .Where(x => !x.IsDead).ToList();
                    }
                case ActionApplying.OnAllOfUnits:
                default:
                    {
                        return battleData.UnitsQueue.Where(x => !x.IsDead).ToList();
                    }
            }
        }

        /// <summary>
        /// Возвращает список целей в направлении атаки (строка,колонка, все цели)
        /// </summary>
        /// <param name="target"></param>
        /// <param name="targets"></param>
        /// <param name="direction"></param>
        /// <returns></returns>
        private static List<Unit> AttackDirectionList(Unit target, List<Unit> targets, AttackDirection direction)
        {
            switch (direction)
            {
                case AttackDirection.OnColumn:
                    return ReturnColumn(target, targets);
                case AttackDirection.OnRow:
                    return ReturnRow(target, targets);
                case AttackDirection.Default:
                default:
                    return targets.Where(x => !x.IsDead).ToList();
            }
        }

        private static List<Unit> ReturnColumn(Unit unit, List<Unit> list)
        {
            if ((int)unit.position % 2 == 0)
            {
                return list.Select(x => x).Where(x => (int)x.position % 2 == 0).Where(z => !z.IsDead).ToList();
            }
            else
            {
                return list.Select(x => x).Where(x => (int)x.position % 2 != 0).Where(z => !z.IsDead).ToList();
            }
        }
        private static List<Unit> ReturnRow(Unit unit, List<Unit> list)
        {
            if ((int)unit.position < 2)
            {
                var temp = list.Select(x => x).Where(x => (int)x.position < 2).Where(z => !z.IsDead).ToList();
                return temp;
            }
            else
            {
                var temp = list.Select(x => x).Where(x => (int)x.position >= 2).Where(z => !z.IsDead).ToList();
                return temp;
            }
        }
    }
}
