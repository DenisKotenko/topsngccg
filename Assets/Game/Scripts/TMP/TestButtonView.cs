using Adic;
using Game.Scripts.Base;
using Game.Scripts.Controllers.BattleController;
using Game.Scripts.Controllers.InputController;

namespace Game.Scripts.TMP
{
    public class TestButtonView:CCGView
    {
        [Inject] private IBattleCommand battleCommand;
        [Inject] private IInputCommand inputCommand;

        public void TestAction()
        {
            inputCommand.ClearSelected();
            battleCommand.NextBattleStep();    
        }
        
    }
}