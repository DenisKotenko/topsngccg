
using System;
using System.Collections.Generic;
using Game.Scripts.Types;

namespace Game.Scripts.Structs
{
    [Serializable]
    public struct BattleData
    {
        public Unit [] Comrades { get; } 
        public Unit [] Enemies { get; }

        public BattleData(Unit [] comrades,Unit [] enemies)
        {
            Comrades = comrades;
            Enemies = enemies;
            
            
        }

        

    }
}