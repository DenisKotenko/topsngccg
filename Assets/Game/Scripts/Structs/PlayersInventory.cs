using System;
using System.Collections.Generic;
using Game.Scripts.Data;
using Game.Scripts.Types;
using UnityEngine;
namespace Game.Scripts.Structs
{

    public static class SystemExtension
    {
        public static T Clone<T>(this T source)
        {
            var serialized = UnityEngine.JsonUtility.ToJson(source);
            return UnityEngine.JsonUtility.FromJson<T>(serialized);
        }
    }

    [Serializable]
    public class PlayersInventory
    {
        private int baseLightValue;
        private int maxBaseLightValue;
        private List<Card> cards;
        private List<Unit> reserveUnits;
        private List<Unit> selectedUnits;

        public int BaseLightValue => baseLightValue;
        public List<Card> Cards => cards;
        public List<Unit> ReserveUnits => reserveUnits;
        public List<Unit> SelectedUnits => selectedUnits;

        public Hero Hero { get; }

        public PlayersInventory(InventorySettings settings)
        {
            Hero = settings.Hero;
            baseLightValue = settings.BaseLightValue;
            maxBaseLightValue = settings.BaseLightValue;
            //Cards = settings.Cards;
            //ReserveUnits = settings.ReserveUnits;
            //SelectedUnits = settings.SelectedUnits;
            cards = new List<Card>();
            reserveUnits = new List<Unit>();
            selectedUnits = new List<Unit>();
            CreateCards(settings.Cards);
            CreateReserveUnits(settings.ReserveUnits);
            CreateSelectedUnits(settings.SelectedUnits);
            InitializeUnits();
        }

        private void CreateSelectedUnits(List<Unit> selectedUnits)
        {
            foreach (var unit in selectedUnits)
            {
                SelectedUnits.Add(unit.Clone());
            }
        }

        private void CreateReserveUnits(List<Unit> reserveUnits)
        {
            foreach (var unit in reserveUnits)
            {
                ReserveUnits.Add(unit.Clone());
            }
        }

        private void CreateCards(List<Card> cards)
        {
            foreach (Card card in cards)
            {
                Cards.Add(card.Clone());
            }
        }

        private void InitializeUnits()
        {
            foreach (Unit unit in SelectedUnits)
            {
                unit.InitDataFromSettings();
            }
        }
        public void AddSelectedUnitToList(Unit unit, int index)
        {
            selectedUnits[index] = unit;
        }
        public void RemoveSelectedUnitFromList(int index)
        {
            selectedUnits[index] = null;
        }

        public void AddReserveUnitToList(Unit unit, int index)
        {
            reserveUnits.Add(unit);
        }

        public void RemoveReserveUnitFromList(Unit unit)
        {
            if (unit != null && reserveUnits.Contains(unit))
                reserveUnits.Remove(unit);
        }

        public void DecreaseLight(int value)
        {
            if ((baseLightValue - value) < 0)
            {
                MonoBehaviour.print($"YOU'R Light is Ended!");
                return;
            }
            baseLightValue -= value;
            MonoBehaviour.print($"Base Light Value is : {BaseLightValue}");
        }
        public void IncreaseLight(int value,bool increaseMax = false)
        {
            if (increaseMax) maxBaseLightValue += value;
            if(value+baseLightValue>=maxBaseLightValue)
            {
                baseLightValue = maxBaseLightValue;
            }
            else
            {
                baseLightValue += value;
            }
        }
    }
}