using System;
using Game.Scripts.Data;
using Game.Scripts.Structs;

namespace Game.Scripts.Controllers.SceneController
{
    public interface ISceneListener
    {
        event Action<string> OnSceneLoad;
        event Action<string> OnSceneUnload;
        event Action OnBattleSceneLoad;
        event Action OnBattleSceneUnload;
        event Action OnInventorySceneLoad;
        event Action OnInventorySceneUnload;
        event Action OnGlobalMapSceneLoad;
        event Action OnGlobalMapSceneUnload;
        event Action OnUISceneLoad;
        event Action OnUISceneUnload;
    }
}