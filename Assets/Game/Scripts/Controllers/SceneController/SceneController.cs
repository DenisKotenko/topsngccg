using System;
using System.Collections;
using System.Collections.Generic;
using Adic;
using Game.Scripts.Base;
using Game.Scripts.Controllers.GameStateController;
using Game.Scripts.Data;
using Game.Scripts.Enums;
using Game.Scripts.Structs;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Scripts.Controllers.SceneController
{
    public sealed class SceneController : ISceneData, ISceneCommand, ISceneListener
    {
        #region Injections

        [Inject] private ScenesSettings _scenesSettings;
        [Inject] private IGameStateListener gameStateListener;
        [Inject] private Coroutiner coroutiner;

        #endregion

        #region Listener

        public event Action<string> OnSceneLoad = path => { };
        public event Action<string> OnSceneUnload = path => { };
        public event Action OnBattleSceneLoad = () => { };
        public event Action OnInventorySceneLoad = () => { };
        public event Action OnGlobalMapSceneLoad = () => { };
        public event Action OnGlobalMapSceneUnload = () => { };
        public event Action OnUISceneLoad = () => { };
        public event Action OnBattleSceneUnload = () => { };
        public event Action OnInventorySceneUnload = () => { };
        public event Action OnUISceneUnload = () => { };

        #endregion

        private List<Scene> activeScenes = new List<Scene>();

        [Inject]
        private void Init()
        {
            gameStateListener.OnGameStateChange += OnGameStateChange;
            coroutiner.CoroutineStart(LoadUIScene());
        }

        private void OnGameStateChange(GameState gameState)
        {
            UnloadActiveScene();
            switch (gameState)
            {
                case GameState.Battle:
                    coroutiner.CoroutineStart(LoadScene(_scenesSettings.BattleScene.Path));
                    break;
                case GameState.GlobalMap:
                    coroutiner.CoroutineStart(LoadScene(_scenesSettings.GlobalMapScene.Path));
                    break;
                case GameState.Event:
                    coroutiner.CoroutineStart(LoadScene(_scenesSettings.EventScene.Path));
                    break;
                case GameState.Inventory:
                    coroutiner.CoroutineStart(LoadInventory());
                    break;
                default:
                    break;
            }
        }

        private IEnumerator LoadInventory()
        {
            var asyncOperation = SceneManager.LoadSceneAsync(_scenesSettings.InventoryScene.Path, LoadSceneMode.Additive);
            yield return new WaitUntil(() => asyncOperation.isDone);

            SceneManager.SetActiveScene(SceneManager.GetSceneByPath(_scenesSettings.InventoryScene.Path));
            activeScenes.Add(SceneManager.GetActiveScene());
            OnInventorySceneLoad.Invoke();
        }

        private SceneController()
        {
            
        }

        private IEnumerator LoadScene(string path)
        {
            var asyncOperation = SceneManager.LoadSceneAsync(path, LoadSceneMode.Additive);
            yield return new WaitUntil(() => asyncOperation.isDone);
            Scene scene = SceneManager.GetSceneByPath(path);
            SceneManager.SetActiveScene(scene);
            activeScenes.Add(scene);

            OnSceneLoad?.Invoke(path);
        }

        private IEnumerator UnloadScene(string path)
        {
            var asyncOperation = SceneManager.UnloadSceneAsync(path);
            yield return new WaitUntil(() => asyncOperation.isDone);

            OnSceneUnload?.Invoke(path);
        }

        private IEnumerator LoadUIScene()
        {
            var asyncOperation = SceneManager.LoadSceneAsync(_scenesSettings.UIScene.Path, LoadSceneMode.Additive);
            yield return new WaitUntil(() => asyncOperation.isDone);

            SceneManager.SetActiveScene(SceneManager.GetSceneByPath(_scenesSettings.UIScene.Path));
            OnUISceneLoad?.Invoke();
        }

        private IEnumerator UnloadUIScene()
        {
            var asyncOperation = SceneManager.UnloadSceneAsync(_scenesSettings.UIScene.Path);
            yield return new WaitUntil(() => asyncOperation.isDone);

            OnUISceneUnload?.Invoke();
        }
        
        private void UnloadActiveScene()
        {
            for (int i = activeScenes.Count-1; i >=0 ; i--)
            {
                Scene activeScene = activeScenes[i];
                activeScenes.Remove(activeScene);
                coroutiner.StartCoroutine(UnloadScene(activeScene.path));
            }
        }

    }
}