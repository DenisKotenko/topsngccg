﻿using Game.Scripts.Enums;
using Game.Scripts.Types;

namespace Game.Scripts.Controllers.GameStateController
{
    public interface IGameStateCommand
    {
        void ChangeGameState(GameState gameState);
    }
}