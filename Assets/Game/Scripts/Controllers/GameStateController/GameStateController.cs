﻿using System;
using Adic;
using Game.Scripts.Enums;
using UnityEngine;

namespace Game.Scripts.Controllers.GameStateController
{
    public sealed class GameStateController : IGameStateCommand, IGameStateData, IGameStateListener
    {
        public GameState CurrentGameState { get; private set; }


        public event Action<GameState> OnGameStateChange = currentState => { };

        private GameStateController()
        {
        }

        public void ChangeGameState(GameState gameState)
        {
            CurrentGameState = gameState;
            OnGameStateChange(CurrentGameState);
        }
    }
}