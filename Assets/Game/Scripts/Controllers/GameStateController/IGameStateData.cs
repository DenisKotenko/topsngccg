﻿using Game.Scripts.Types;
using Game.Scripts.Enums;

namespace Game.Scripts.Controllers.GameStateController
{
    public interface IGameStateData
    {
        GameState CurrentGameState { get; }
    }
}