﻿using Game.Scripts.Enums;
using System;

namespace Game.Scripts.Controllers.GameStateController
{
    public interface IGameStateListener
    {
        event Action<GameState> OnGameStateChange;
    }
}