﻿using Game.Scripts.Data;

namespace Game.Scripts.Controllers.EventController
{
    public interface IEventData
    {
        EventSettings CurrentEvent { get; }
        QuestionSettings CurrentQuestion { get; }
        AnswerSettings CurrentAnswer { get; }
    }
}
