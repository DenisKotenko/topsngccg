﻿using Game.Scripts.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Scripts.Controllers.EventController
{
    public interface IEventCommand
    {
        void LoadNextQuestion();
        void SelectAnswer(AnswerSettings answer);
    }
}
