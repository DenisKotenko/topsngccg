﻿using Adic;
using Game.Scripts.Controllers.GameStateController;
using Game.Scripts.Controllers.GlobalMapController;
using Game.Scripts.Controllers.PlayersController;
using Game.Scripts.Controllers.SceneController;
using Game.Scripts.Data;
using Game.Scripts.Enums;
using System;

namespace Game.Scripts.Controllers.EventController
{
    public sealed class EventController : IEventData, IEventListener, IEventCommand
    {
        [Inject] private ISceneListener sceneListener;
        [Inject] private IGameStateData gameStateData;
        [Inject] private IGlobalMapData globalMapData;
        [Inject] private IGameStateCommand gameStateCommand;
        [Inject] private IPlayerCommand playerCommand;

        private EventSettings currentEvent;
        private QuestionSettings currentQuestion;
        private AnswerSettings currentAnswer;

        public EventSettings CurrentEvent => currentEvent;
        public QuestionSettings CurrentQuestion => currentQuestion;
        public AnswerSettings CurrentAnswer => currentAnswer;

        public event Action OnQuestionLoad = () => { };

        [Inject]
        private void Init()
        {
            Listen();
        }

        private EventController()
        {
        }

        private void Listen()
        {
            sceneListener.OnSceneLoad += OnSceneLoad;
        }

        private void OnSceneLoad(string path)
        {
            if (gameStateData.CurrentGameState == GameState.Event)
            {
                LoadEvent();
            }
        }

        private void LoadEvent()
        {
            currentEvent = globalMapData.CurrentMapPoint.EventSettings;
            LoadNextQuestion();
        }

        public void LoadNextQuestion()
        {
            if (isFinalAnswer(currentAnswer))
            {
                playerCommand.GetRewards(currentAnswer.RewardSettings);
                gameStateCommand.ChangeGameState(GameState.GlobalMap);
            }
            else
            {
                QuestionStruct questionStruct = currentEvent.Questions.Find(q1 => { return q1.Answers.FindAll(a1 => { return a1 == currentAnswer; }).Count > 0; });
                currentQuestion = questionStruct.Question;
                OnQuestionLoad?.Invoke();
            }
        }

        private bool isFinalAnswer(AnswerSettings answer)
        {
            if (answer == null)
            {
                return false;
            }
            else
            {
                return answer.FinalAnswer;
            }
        }

        public void SelectAnswer(AnswerSettings answer)
        {
            currentAnswer = answer;
        }
    }
}
