using Game.Scripts.Types;
using System;

namespace Game.Scripts.Controllers.PlayersController
{
    public interface IPlayerListener
    {
        event Action OnInventoryRefresh;
        event Action OnButtonClick;
        event Action<Unit> OnUnitSelect;
    }
}