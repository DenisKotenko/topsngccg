using System;
using Adic;
using System.Collections.Generic;
using Game.Scripts.Data;
using Game.Scripts.Enums;
using Game.Scripts.Structs;
using Game.Scripts.Types;
using UnityEngine;

namespace Game.Scripts.Controllers.PlayersController
{
    public sealed class PlayerController : IPlayerCommand, IPlayerData, IPlayerListener
    {
        [Inject] private InventorySettings inventorySettings;
        private PlayersInventory inventory;
        public PlayersInventory Inventory => inventory;
        public event Action OnInventoryRefresh = () => { };
        public event Action OnButtonClick = () => { };
        public event Action<Unit> OnUnitSelect = unit => { };

        public int BaseLightValue => inventory.BaseLightValue;
        public List<Unit> SelectedUnits => inventory.SelectedUnits;
        public List<Unit> ReserveUnits => inventory.ReserveUnits;

        [Inject]
        private void Init()
        {
            inventory = new PlayersInventory(inventorySettings);
        }

        public void AddSelectedUnitToList(Unit unit, int index)
        {
            inventory.SelectedUnits[index] = unit;
            OnInventoryRefresh();
        }
        public void RemoveSelectedUnitFromList(int index)
        {
            inventory.SelectedUnits[index] = null;
            OnInventoryRefresh();
        }
        public void AddReserveUnitToList(Unit unit)
        {
            inventory.ReserveUnits.Add(unit);
            OnInventoryRefresh();
        }
        public void RemoveReserveUnitFromList(Unit unit)
        {
            inventory.ReserveUnits.Remove(unit);
            OnInventoryRefresh();
        }

        public void ChangePosition(Unit unit, UnitPosition position)
        {
            unit.position = position;
        }

        public void ButtonClick()
        {
            OnButtonClick();
        }

        public void GetRewards(RewardSettings reward)
        {
            foreach (UnitSettings unitSettings in reward.ListOfUnits)
            {
                inventory.ReserveUnits.Add(new Unit(unitSettings));
            }
            foreach (CardSettings cardSettings in reward.ListOfCards)
            {
                inventory.Cards.Add(new Card(cardSettings));
            }
        }
        public void DecreaseLight(int value)
        {
            inventory.DecreaseLight(value);
        }

        public void IncreaseLight(int value, bool increaseMax = false)
        {
            inventory.IncreaseLight(value,increaseMax);
        }

        public void LoadCardsFromDeck(Unit unit)
        {
            OnUnitSelect.Invoke(unit);
        }

        private PlayerController()
        {
        }
    }
}