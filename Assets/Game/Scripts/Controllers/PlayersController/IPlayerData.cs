using System.Collections.Generic;
using Game.Scripts.Structs;
using Game.Scripts.Types;

namespace Game.Scripts.Controllers.PlayersController
{
    public interface IPlayerData
    {
        int BaseLightValue { get; }
        List<Unit> SelectedUnits { get; }
        List<Unit> ReserveUnits { get; }
    }
}