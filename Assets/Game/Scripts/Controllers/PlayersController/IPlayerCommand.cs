using Game.Scripts.Data;
using Game.Scripts.Enums;
using Game.Scripts.Types;

namespace Game.Scripts.Controllers.PlayersController
{
    public interface IPlayerCommand
    {
        void AddSelectedUnitToList(Unit unit, int index);
        void RemoveSelectedUnitFromList(int index);
        void AddReserveUnitToList(Unit unit);
        void RemoveReserveUnitFromList(Unit unit);
        void ChangePosition(Unit unit, UnitPosition position);
        void ButtonClick();
        void LoadCardsFromDeck(Unit unit);

        void GetRewards(RewardSettings reward);
        void DecreaseLight(int value);

    }
}