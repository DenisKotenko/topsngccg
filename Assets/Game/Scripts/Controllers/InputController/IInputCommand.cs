﻿using System;
using Adic;
using Game.Scripts.Enums;
using Game.Scripts.Types;
using UnityEngine;

namespace Game.Scripts.Controllers.InputController
{
    public interface IInputCommand
    {
        void ChangeSelectedObject(CCGType selectedObject);
        void ClearSelected();
    }
}