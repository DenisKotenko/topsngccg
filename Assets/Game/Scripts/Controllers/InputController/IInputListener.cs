﻿using System;
using Adic;
using Game.Scripts.Enums;
using Game.Scripts.Types;
using UnityEngine;

namespace Game.Scripts.Controllers.InputController
{
    public interface IInputListener
    {
        event Action<CCGType> OnSelectedObjectChange;
    }
}