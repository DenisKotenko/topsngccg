﻿using System;
using Adic;
using Game.Scripts.Enums;
using Game.Scripts.Types;
using UnityEngine;

namespace Game.Scripts.Controllers.InputController
{
    public sealed class InputController : IInputListener, IInputData, IInputCommand
    {
        private CCGType selectedObject;
        private CCGType previousSelectedObject;

        public CCGType SelectedObject => selectedObject;

        public CCGType PreviousSelectedObject => previousSelectedObject;

        public event Action<CCGType> OnSelectedObjectChange = (CCGType selectedObject) => { };

        public void ChangeSelectedObject(CCGType selectedObject)
        {
            previousSelectedObject = this.selectedObject;
            this.selectedObject = selectedObject;
            OnSelectedObjectChange(selectedObject);
        }

        public void ClearSelected()
        {
            selectedObject = null;
            previousSelectedObject = null;
            OnSelectedObjectChange(null);
        }
    }
}