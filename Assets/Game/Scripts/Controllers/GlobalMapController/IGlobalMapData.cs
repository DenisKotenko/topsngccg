﻿using Game.Scripts.Data;

namespace Game.Scripts.Controllers.GlobalMapController
{
    public interface IGlobalMapData
    {
        MapPointSettings CurrentMapPoint { get; }
    }
}
