﻿using System;

namespace Game.Scripts.Controllers.GlobalMapController
{
    public interface IGlobalMapListener
    {
        event Action OnChangeCurrentMapPoint;
    }
}
