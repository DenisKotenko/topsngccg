﻿using Game.Scripts.Data;

namespace Game.Scripts.Controllers.GlobalMapController
{
    public interface IGlobalMapCommand
    {
        void ChangeCurrentMapPoint(MapPointSettings mapPointSettings);
        bool CanChangePoint(MapPointSettings targetPoint);
        void SetCurrentMapPointPassed();
    }
}
