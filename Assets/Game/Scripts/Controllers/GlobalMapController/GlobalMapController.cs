﻿using System;
using Game.Scripts.Data;
using UnityEngine;

namespace Game.Scripts.Controllers.GlobalMapController
{
    public sealed class GlobalMapController : IGlobalMapCommand, IGlobalMapData, IGlobalMapListener
    {
        #region Data

        public MapPointSettings CurrentMapPoint => currentMapPoint;
        private MapPointSettings currentMapPoint = ScriptableObject.CreateInstance<MapPointSettings>();

        #endregion

        #region Listener

        public event Action OnChangeCurrentMapPoint = () => { };

        #endregion

        #region Command

        public void ChangeCurrentMapPoint(MapPointSettings mapPointSettings)
        {
            currentMapPoint = mapPointSettings;
            if ((currentMapPoint.ListOfPreviousPoints.Count == 0) || (currentMapPoint.MapPointType != Enums.MapPointType.Battle))
            {
                SetCurrentMapPointPassed();
            }
        }

        public bool CanChangePoint(MapPointSettings targetPoint)
        {
            return (targetPoint.ListOfPreviousPoints.Count == 0) || (targetPoint.ListOfPreviousPoints.Contains(currentMapPoint) & currentMapPoint.IsPassed) || (targetPoint == currentMapPoint & !currentMapPoint.IsPassed);
        }

        public void SetCurrentMapPointPassed()
        {
            currentMapPoint.IsPassed = true;
        }

        #endregion
    }
}
