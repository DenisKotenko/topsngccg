﻿using Game.Scripts.Enums;
using System;

namespace Game.Scripts.Controllers.UIController
{
    public interface IUIListener
    {
        event Action<GameState> OnUIChanged;
    }
}
