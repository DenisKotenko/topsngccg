﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Adic;
using Game.Scripts.Data;
using Game.Scripts.Enums;
using Game.Scripts.Controllers.GameStateController;
using System;

namespace Game.Scripts.Controllers.UIController
{
    public class UIController : IUICommand, IUIData, IUIListener
    {
        [Inject] private IGameStateListener gameStateListener;

        public event Action<GameState> OnUIChanged = currentState => { };

        [Inject]
        protected void Init()
        {
            gameStateListener.OnGameStateChange += OnGameStateChange;
        }

        private void OnGameStateChange(GameState gameState)
        {
            OnUIChanged?.Invoke(gameState);
        }

        private void OnDestroy()
        {
            gameStateListener.OnGameStateChange -= OnGameStateChange;
        }

    }

}

