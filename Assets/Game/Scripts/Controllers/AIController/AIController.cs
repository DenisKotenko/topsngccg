﻿using Adic;
using Game.Scripts.Controllers.BattleController;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Game.Scripts.Types;

namespace Game.Scripts.Controllers.AIController
{
    public class AIController : IAICommand, IAIData, IAIListener, IDisposable
    {
        [Inject] private IBattleListener battleListener;
        [Inject] private IBattleCommand battleCommand;
        [Inject] private IBattleData battleData;

        [Inject]
        private void Init()
        {
            battleListener.OnEnemyTurn += Turn;
        }

        private void Turn(Unit unit)
        {
            MonoBehaviour.print($"enemy turn");
            if (unit.ActiveDeck.Count() != 0)
            {
                ExecuteCard(unit);
            }
            else
            {
                MonoBehaviour.print($"pass");
            }
            battleCommand.NextBattleStep();
        }

        private void ExecuteCard(Unit unit)
        {
            if (unit.ActiveDeck.Count() == 0) return;
            int randIndex = UnityEngine.Random.Range(0, unit.ActiveDeck.Count());

            Card card = unit.ActiveDeck[randIndex];
            if (!card.DataNotNull) return;

            var result = battleData.Comrades.Where(x => x != null && !x.IsDead);
            Unit comradeUnit = result.ElementAt(UnityEngine.Random.Range(0, result.Count()));
            MonoBehaviour.print($"Use card - {card} on unit {comradeUnit.Name} ");
            if (battleData.CurrentUnit.BaseLightCount - card.BaseLightCost < 0 ||
                battleData.CurrentUnit.ExtraLightCount - card.ExtraLightCost < 0)
            {
                MonoBehaviour.print("*-*-*-*-Enemy has no Light!-*-*-*-*");
                return;
            }
            card.Execute(comradeUnit);
                // battleCommand.DiscardCard(card);     //продумать AI ( я думаю у него не будетп       
        }

        public void Dispose()
        {
            battleListener.OnEnemyTurn -= Turn;
        }
    }
}
