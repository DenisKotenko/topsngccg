
using System.Collections.Generic;
using Game.Scripts.Types;

namespace Game.Scripts.Controllers.BattleController
{
    public interface IBattleData
    {
        List<Unit> Comrades { get; }
        List<Unit> Enemies { get; }
        List<Unit> UnitsQueue { get; }
        int RoundCount { get; }
        Unit CurrentUnit { get; }
    }
}