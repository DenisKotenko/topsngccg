using System;
using System.Collections.Generic;
using Game.Scripts.Types;

namespace Game.Scripts.Controllers.BattleController
{
    public interface IBattleListener
    {
        event Action OnBattleStart;
        event Action OnRoundStart;
        event Action<Unit> OnTurnStart;
        event Action<Unit> OnPlayerTurn;
        event Action<Unit> OnEnemyTurn;
        event Action OnBattleDataLoaded;
        event Action<Card> OnCardDiscarded;
        event Action<List<Card>> OnComradeCardMovedToActiveDeck;
        event Action<List<Card>> OnEnemyCardMovedToActiveDeck;

        event Action<Unit> OnUnitDataChange;

    }
}