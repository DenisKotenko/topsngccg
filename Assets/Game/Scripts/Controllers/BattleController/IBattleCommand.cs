using Game.Scripts.Types;
using System.Collections.Generic;
namespace Game.Scripts.Controllers.BattleController
{
    public interface IBattleCommand
    {
        void NextBattleStep();
        void DiscardCard(Card card);
        void SetHeal(Unit unit, int value);
        void SetDamage(Unit unit, int value);
        void IncreaseLight(List<Unit> units, int value, bool increaseMax = false);
        void DecreaseLight(int baseLightValue, int extraLightValue);
    }
}