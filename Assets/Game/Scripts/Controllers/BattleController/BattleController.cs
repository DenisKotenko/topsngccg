using System;
using System.Collections.Generic;
using System.Linq;
using Adic;
using Game.Scripts.Controllers.GameStateController;
using Game.Scripts.Controllers.GlobalMapController;
using Game.Scripts.Controllers.InputController;
using Game.Scripts.Controllers.PlayersController;
using Game.Scripts.Controllers.SceneController;
using Game.Scripts.Data;
using Game.Scripts.Enums;
using Game.Scripts.Types;
using UnityEngine;

namespace Game.Scripts.Controllers.BattleController
{
    public sealed class BattleController : IBattleData, IBattleListener, IBattleCommand, IDisposable
    {
        #region Injections

        [Inject] private ISceneListener sceneListener;
        [Inject] private IInputData inputData;
        [Inject] private GlobalBattlesSettings globalBattleSettings;
        [Inject] private IPlayerData playerData;
        [Inject] private IPlayerCommand playerCommand;
        [Inject] private IGlobalMapData globalMapData;
        [Inject] private IGlobalMapCommand globalMapCommand;
        [Inject] private IGameStateData gameStateData;
        [Inject] private IGameStateCommand gameStateCommand;

        #endregion

        #region Listener

        public event Action OnBattleStart = () => { };
        public event Action OnRoundStart = () => { };
        public event Action<Unit> OnTurnStart = unit => { };
        public event Action<Unit> OnPlayerTurn = unit => { };
        public event Action<Unit> OnEnemyTurn = unit => { };
        public event Action OnBattleDataLoaded = () => { };
        public event Action<Card> OnCardDiscarded = settings => { };
        public event Action<List<Card>> OnComradeCardMovedToActiveDeck = settings => { };
        public event Action<List<Card>> OnEnemyCardMovedToActiveDeck = settings => { };
        public event Action<Unit> OnUnitDataChange = unit => { };

        #endregion

        #region Data

        public List<Unit> Comrades => comrades;
        public List<Unit> Enemies => enemies;
        public List<Unit> UnitsQueue => unitQueue;
        public int RoundCount { get; private set; }

        public Unit CurrentUnit => currentTurnUnit;

        #endregion

        private List<Unit> enemies = new List<Unit>();
        private List<Unit> comrades = new List<Unit>();
        private List<Unit> unitQueue = new List<Unit>();
        private int currentUnitIndex;
        private int roundCount;
        private Unit currentTurnUnit;

        [Inject]
        private void Init()
        {
            Listen();
        }

        private BattleController()
        {
        }

        private void Listen()
        {
            sceneListener.OnSceneLoad += OnSceneLoad;
        }

        private void OnSceneLoad(string path)
        {
            if (gameStateData.CurrentGameState == GameState.Battle)
            {
                onBattleLoad();
            }
        }

        private void onBattleLoad()
        {
            LoadBattle();
            LoadDeckData();
            OnBattleDataLoaded?.Invoke();
            OnBattleStart?.Invoke();
            StartRound();
            StartTurn();
        }

        public void NextBattleStep()
        {
            if (currentUnitIndex < unitQueue.Count - 1)
            {
                currentUnitIndex++;
                StartTurn();
            }
            else
            {
                roundCount++;
                currentUnitIndex = 0;
                StartRound();
                StartTurn();
            }
        }

        private void LoadDeckData()
        {
            InitGameDecks(comrades);
            InitGameDecks(enemies);
        }

        private void InitGameDecks(List<Unit> units)
        {
            foreach (var unit in units)
            {
                if (unit != null)
                {
                    unit.InitializeGameDeck();
                }
            }
        }

        private void LoadBattle()
        {
            comrades = playerData.SelectedUnits.ToList();
            enemies = globalMapData.CurrentMapPoint.BattleSettings.Enemies.ToList();
            foreach (var unit in comrades)
            {
                if (unit != null)
                {
                    unitQueue.Add(unit);
                }
            }

            // foreach (var unit in enemies)
            for (int i = 0; i < enemies.Count; i++)
            {
                if (enemies[i] != null)
                {
                    enemies[i].InitDataFromSettings();
                    enemies[i].position = (UnitPosition)i;
                    unitQueue.Add(enemies[i]);
                }
            }

            unitQueue.Sort((a, b) => -1 * a.Initiate.CompareTo(b.Initiate));
        }


        private void StartTurn()
        {
            if (currentTurnUnit != null)
            {
                CheckWin();
            }
            currentTurnUnit = unitQueue[currentUnitIndex];

            MonoBehaviour.print($"Turn {currentUnitIndex} started ");
            OnTurnStart.Invoke(currentTurnUnit);
            MonoBehaviour.print("---------------------- ");
            if (currentTurnUnit.IsStunned)
            {
                currentTurnUnit.RemoveStun();
                MonoBehaviour.print($"-*-*-*Unit {currentTurnUnit.Name} Is Stunned!-*-*-*");
                NextBattleStep();
                return;
            }
            if (comrades.Contains(currentTurnUnit))
            {
                OnPlayerTurn?.Invoke(currentTurnUnit);
            }
            else if (enemies.Contains(currentTurnUnit))
            {
                OnEnemyTurn?.Invoke(currentTurnUnit);
            }
        }

        private void StartRound()
        {
            DealTheCardsToComrades();
            DealTheCardsToEnemies();
            foreach (Unit comrade in comrades.Where(x => x != null && !x.IsDead))
            {
                playerCommand.DecreaseLight(comrade.UnitLightCost);
            }

            IncreaseLight(unitQueue.Where(x => x != null && !x.IsDead).ToList(), roundCount);
            MonoBehaviour.print($"==========================");
            MonoBehaviour.print($"Round {roundCount} started");

            OnRoundStart.Invoke();
        }


        private void DealTheCardsToComrades()
        {

            foreach (var unit in comrades)
            {
                if (unit != null)
                {
                    if (unit.IsDead) continue; ;
                    OnComradeCardMovedToActiveDeck(unit.DealTheCards(globalBattleSettings.MaxCardsInHandByOneUnit));
                }
            }
        }

        private void DealTheCardsToEnemies()
        {
            foreach (var unit in enemies)
            {
                if (unit.IsDead) continue;
                OnEnemyCardMovedToActiveDeck(unit.DealTheCards(globalBattleSettings.MaxCardsInHandByOneUnit));
            }
        }

        public void Dispose()
        {
            sceneListener.OnSceneLoad -= OnSceneLoad;
        }

        public void DiscardCard(Card card)
        {
            currentTurnUnit.MoveCardFromActiveToDiscardPileDeck(card);
            OnCardDiscarded(card);
        }


        private void CheckWin()
        {
            var comradeLives = comrades.Where(x => x != null && !x.IsDead);
            var enemyLives = enemies.Where(x => !x.IsDead);
            string status = "";
            if (comradeLives.Count() == 0)
            {
                status = "lose";
            }
            if (enemyLives.Count() == 0)
            {
                status = "win";
                globalMapCommand.SetCurrentMapPointPassed();
            }
            if (status != "")
            {
                MonoBehaviour.print($"You {status}!");
                gameStateCommand.ChangeGameState(GameState.BattleResults);
            }
        }

        private void DisableUnitFromBattle(Unit unit)
        {
            for (int i = unit.ActiveDeck.Count() - 1; i >= 0; i--)
            {
                Card card = unit.ActiveDeck[i];
                unit.MoveCardFromActiveToDiscardPileDeck(card);
                OnCardDiscarded(card);
            }
            unitQueue.Remove(unit);

        }

        public void SetHeal(Unit unit, int value)
        {
            unit.SetHeal(value);
            OnUnitDataChange?.Invoke(unit);

        }

        public void SetDamage(Unit unit, int value)
        {
            if (unit != null)
            {
                unit.SetDamage(value);
                OnUnitDataChange?.Invoke(unit);
                if (unit.IsDead)
                {
                    DisableUnitFromBattle(unit);
                }
                CheckWin();
            }
        }

        public void IncreaseLight(List<Unit> units, int value, bool increaseMax = false)
        {
            foreach (Unit unit in units)
            {
               unit.IncreaseLight(value);
            }
        }
        public void DecreaseLight(int baseLightValue, int extraLightValue)
        {
            currentTurnUnit.DecreaseLight(baseLightValue, extraLightValue);
        }

    }
}
