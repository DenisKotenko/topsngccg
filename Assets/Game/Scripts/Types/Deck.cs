using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Scripts.Types
{
    [Serializable]
    public class Deck : CCGType, IEnumerable
    {
        public Card this[int index] => cards[index];

        [SerializeField] private List<Card> cards = new List<Card>();

        public Deck(Deck copyDeck)
        {
            foreach (Card card in copyDeck)
            {
                Add(card);
            }
        }

        public Deck()
        {
        }

        public IEnumerator GetEnumerator() => cards.GetEnumerator();

        public void Add(Card card)
        {
            cards.Add(card);
        }

        public void Remove(Card card)
        {
            cards.Remove(card);
        }

        public int Count() => cards.Count;

        public void Clear()
        {
            cards.Clear();
        }

        public void Shuffle()
        {
            var random = new System.Random();
            for (int i = cards.Count - 1; i >= 1; i--)
            {
                int j = random.Next(i + 1);
                var temp = cards[j];
                cards[j] = cards[i];
                cards[i] = temp;
            }
        }

        public bool ContainsCard(Card card)
        {
            return cards.Contains(card);
        }
    }
}