using System;
using UnityEngine;

namespace Game.Scripts.Types
{
    [Serializable]
    public class Hero:CCGType
    {

        [SerializeField]
        private string info;
        public string Info => info;

    }
}