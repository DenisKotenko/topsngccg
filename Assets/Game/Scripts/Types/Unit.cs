using Game.Scripts.Enums;
using Game.Scripts.Data;
using System;
using System.Collections.Generic;
//using Game.Scripts.Types.Enums;
using UnityEngine;

namespace Game.Scripts.Types
{
    [Serializable]
    public class Unit : CCGType
    {
        [SerializeField] private UnitSettings unitSettings;
        [SerializeField] private Deck deck;

        private int health;
        private int baseLightCount;
        private int maxBaseLight;
        private int extraLightCount;
        private int maxExtraLight;
        private int unitLightCost;
        private int stunCounter;

        private Deck gameDeck;
        private Deck activeDeck;
        private Deck discardPile;
        public UnitType UnitType => unitSettings.UnitType;
        public Deck Deck => deck;
        public Deck GameDeck => gameDeck;
        public Deck ActiveDeck => activeDeck;
        public Deck DiscardPile => discardPile;

        public string Name => unitSettings.UnitName;
        public int Initiate => unitSettings.Initiate;
        public string Description => unitSettings.Description;

        //Lights
        public int UnitLightCost => unitLightCost;
        public int BaseLightCount => baseLightCount;
        public int ExtraLightCount => extraLightCount;
        public int MaxBaseLight => maxBaseLight;
        public CCGLightType BaseLightType => unitSettings.BaseLightType;
        public int MaxExtraLight => maxExtraLight;
        public CCGLightType ExtraLightType => unitSettings.ExtraLightType;
        //Stats
        public int PhysicalPower => unitSettings.PhysicalPower;
        public int MagicalPower => unitSettings.MagicalPower;
        public int HealPower => unitSettings.HealPower;
        //Resists
        public int PhysicalResist => unitSettings.PhysicalResist;
        public int FireResist => unitSettings.FireResist;
        public int PoisonResist => unitSettings.PoisonResist;
        //Inventory
        [HideInInspector] public UnitPosition position;

        //State
        public int Health => health;
        public bool IsDead => health <= 0;
        public bool IsStunned => stunCounter > 0;
        public int MaxHealth => unitSettings.Health;
        public Unit(UnitSettings settings)
        {
            if (settings == null)
            {
                return;
            }
            unitSettings = settings;
            deck = new Deck();
            gameDeck = new Deck();
            activeDeck = new Deck();
            InitDataFromSettings();
        }

        public Unit()
        {
            deck = new Deck();
            gameDeck = new Deck();
            activeDeck = new Deck();
        }

        public void InitializeGameDeck()
        {
            gameDeck = new Deck(Deck);
            gameDeck.Shuffle();
            activeDeck = new Deck();
            discardPile = new Deck();
        }

        public List<Card> DealTheCards(int cardsQuantityInActive)
        {

            List<Card> listOfCards = new List<Card>();
            int cardsQuantity = cardsQuantityInActive - activeDeck.Count();
            if (cardsQuantity > 0)
            {
                if (cardsQuantity > gameDeck.Count())
                {
                    ReturnCardsToTheGameDeckFromDiscardPile();
                    ShuffleGameDeck();
                }

                for (int i = 0; i < cardsQuantity; i++)
                {
                    if (gameDeck.Count() == 0)
                    {
                        //MonoBehaviour.print("Not enough quantity cards for move to active deck. Do nothing.");
                        break;
                    }
                    Card card = gameDeck[0];
                    gameDeck.Remove(card);
                    activeDeck.Add(card);
                    listOfCards.Add(card);
                }
            }
            return listOfCards;
        }

        public void MoveCardFromActiveToDiscardPileDeck(Card card)
        {
            activeDeck.Remove(card);
            discardPile.Add(card);
        }

        public void ReturnCardsToTheGameDeckFromDiscardPile()
        {
            foreach (Card card in discardPile)
            {
                gameDeck.Add(card);
            }
            discardPile.Clear();
        }

        public void ShuffleGameDeck()
        {
            gameDeck.Shuffle();
        }

        public void SetDamage(int value)
        {
            health -= value;
        }

        public void InitDataFromSettings()
        {
            health = unitSettings.Health;
            baseLightCount = unitSettings.BaseLight;
            extraLightCount = unitSettings.ExtraLight;
            maxBaseLight = unitSettings.BaseLight;
            maxExtraLight = unitSettings.ExtraLight;
            unitLightCost = unitSettings.LightCost;
            stunCounter = 0;
        }
        public void SetHeal(int value)
        {
            if (health + value > MaxHealth)
            {
                health = MaxHealth;
                return;
            }
            health += value;
        }
        public void IncreaseLight(int value, bool increaseMax = false)
        {
            if (increaseMax)
            {
                maxBaseLight += value;
                maxExtraLight += value;
                return;
            }

            int tempA = baseLightCount;
            int tempB = extraLightCount;
            if ((baseLightCount + value) > maxBaseLight)
            {
                baseLightCount = maxBaseLight;
            }
            else
            {
                baseLightCount += value;
            }

            if ((extraLightCount + value) > maxExtraLight)
            {
                extraLightCount = maxExtraLight;
            }
            else
            {
                extraLightCount += value;
            }
            MonoBehaviour.print($"Was light value: {tempA},{tempB} - New Light value is: {baseLightCount},{extraLightCount}");

        }

        public void DecreaseLight(int baseLightValue, int extraLightValue)
        {
            baseLightCount -= baseLightValue;
            extraLightCount -= extraLightValue;
            if (baseLightCount <= 0) baseLightCount = 0;
            if (extraLightCount <= 0) extraLightCount = 0;
            MonoBehaviour.print($"{baseLightCount} , {extraLightCount} -- Light on {this.Name}!!!! ");

        }

        public void StunUnit(int roundsCount)
        {
            stunCounter = roundsCount;
            
        }

        public void RemoveStun(int value = 0)
        {
            if (value > 0)
            {
                stunCounter -= value;
                if (stunCounter < 0) stunCounter = 0;
            }
           stunCounter--;
        }
    }
}