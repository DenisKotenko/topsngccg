using System;
using System.Collections.Generic;
using Adic;
using Game.Scripts.Enums;
using Game.Scripts.Base;
using Game.Scripts.Controllers.BattleController;
using Game.Scripts.Data;
using Game.Scripts.Data.Actions;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Game.Scripts.Types
{
    [Serializable]
    public class Card : CCGType
    {
        [Required]
        [SerializeField] private CardSettings cardsettings;

        [Inject] private IBattleCommand battleCommand;
        public string Name => cardsettings.CardName;
        public string Description => cardsettings.Description;
        public int BaseLightCost => cardsettings.BaseLightCost;
        public int ExtraLightCost => cardsettings.ExtraLightCost;
        public CCGLightType BaseLightType => cardsettings.BaseLightType;
        public CCGLightType ExtraLightType => cardsettings.ExtraLightType;
        public override string ToString() => Name;
        public bool DataNotNull => cardsettings != null;
        public List<CardActionSettings> Actions => cardsettings.Actions;

        public Card(CardSettings settings)
        {
            cardsettings = settings;
        }
        public void Execute(Unit target)
        {
            battleCommand = GameContextRoot.Container.Resolve<IBattleCommand>();
            foreach (CardActionSettings action in Actions)
            {
                if (!action.Execute(target)) return;
            }

            battleCommand.DecreaseLight(BaseLightCost, ExtraLightCost);
            battleCommand.DiscardCard(this);
        }
    }
}