﻿namespace Game.Scripts.Enums
{
    public enum AttackDirection
    {
        Default,
        OnColumn,
        OnRow
    }

    public enum ActionApplying
    {
        Enemies,
        Comrades,
        OnAllOfUnits
    }

    public enum AttackSelection
    {
        Single,
        Massive
    }

    public enum AttackType
    {
        Physical,
        Magic,
        Heal
    }

    public enum DamageType
    {
        Physical,
        Fire,
        Ice,
        Poison
    }


}