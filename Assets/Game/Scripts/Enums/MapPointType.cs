﻿namespace Game.Scripts.Enums
{
    public enum MapPointType
    {
        Battle,
        Town,
        Rest,
        Event
    }
}
