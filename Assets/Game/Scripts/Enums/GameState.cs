﻿namespace Game.Scripts.Enums
{
    public enum GameState
    {
        Battle,
        GlobalMap,
        Inventory,
        BattleResults,
        Event,
        Town,
        Rest
    }
}
