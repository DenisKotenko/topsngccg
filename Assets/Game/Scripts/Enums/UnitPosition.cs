﻿namespace Game.Scripts.Enums
{
    public enum UnitPosition
    {
        MeleOne,
        RangeOne,
        MeleTwo,
        RangeTwo
    }
}
