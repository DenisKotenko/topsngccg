using System;
using System.Collections.Generic;
using Game.Scripts.Base;
using Game.Scripts.Types;
using Game.Scripts.Views.InventoryViews;
using UnityEngine;

namespace Game.Scripts.Data
{
    [Serializable]
    [CreateAssetMenu(fileName = "[Global Data] - Inventory", menuName = "Global Data/Inventory Data")]
    public sealed class InventorySettings : CCGGlobalGameSettings
    {
        [SerializeField] private Hero hero;
        [SerializeField] private int baseLightValue;
        [SerializeField] private List<Unit> reserveUnits;
        [SerializeField] private List<Unit> selectedUnits;
        [SerializeField] private List<Card> cards;
        [SerializeField] private InventoryReserveUnitView reserveUnitPrefab;
        [SerializeField] private InventoryCardView cardPrefab;

        public Hero Hero => hero;
        public int BaseLightValue => baseLightValue;
        public List<Unit> ReserveUnits => reserveUnits;
        public List<Unit> SelectedUnits => selectedUnits;
        public List<Card> Cards => cards;
        public InventoryReserveUnitView ReserveUnitPrefab => reserveUnitPrefab;
        public InventoryCardView CardPrefab => cardPrefab;
    }
}