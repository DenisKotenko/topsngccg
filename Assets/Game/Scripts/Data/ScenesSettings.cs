using System;
using Game.Scripts.Base;
using SFAsset.Attributes.AssetPath;
using UnityEngine;
using UnityEngine.SceneManagement;
using SFAsset;

namespace Game.Scripts.Data
{
    [CreateAssetMenu(fileName = "[Global Data] - Scenes", menuName = "Global Data/Scenes Data")]
    [Serializable]
    public sealed class ScenesSettings : CCGGlobalGameSettings
    {
        [SerializeField] [SFAsset(typeof(Scene))]
        private SFAsset.SFAsset battleScene;

        [SerializeField] [SFAsset(typeof(Scene))]
        private SFAsset.SFAsset inventoryScene;

        [SerializeField] [SFAsset(typeof(Scene))]
        private SFAsset.SFAsset globalMapScene;

        [SerializeField] [SFAsset(typeof(Scene))]
        private SFAsset.SFAsset uiScene;

        [SerializeField] [SFAsset(typeof(Scene))]
        private SFAsset.SFAsset eventScene;

        [SerializeField] [SFAsset(typeof(Scene))]
        private SFAsset.SFAsset restScene;

        [SerializeField] [SFAsset(typeof(Scene))]
        private SFAsset.SFAsset townScene;

        public SFAsset.SFAsset BattleScene => battleScene;
        public SFAsset.SFAsset InventoryScene => inventoryScene;
        public SFAsset.SFAsset UIScene => uiScene;
        public SFAsset.SFAsset GlobalMapScene => globalMapScene;
        public SFAsset.SFAsset EventScene => eventScene;
        public SFAsset.SFAsset RestScene => restScene;
        public SFAsset.SFAsset TownScene => townScene;
    }
}