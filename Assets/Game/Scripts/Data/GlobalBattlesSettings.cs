using System;
using Game.Scripts.Views;
using Game.Scripts.Base;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Game.Scripts.Data
{
    [Serializable]
    [CreateAssetMenu(fileName = "[Global Data] - Battles", menuName = "Global Data/Battles Data")]
    public sealed class GlobalBattlesSettings : CCGGlobalGameSettings
    {
        [BoxGroup("Battles")] [SerializeField] private BattleSettings testBattleSettings;
        public BattleSettings TestBattleSettings => testBattleSettings;

        [BoxGroup("Hand Settings")] [MinValue(0)] [SerializeField]
        private int maxCardsInHandByOneUnit;

        public int MaxCardsInHandByOneUnit => maxCardsInHandByOneUnit;

        [BoxGroup("Prefabs")] [SerializeField]
        private BattleCardView cardView;

        public BattleCardView CardView => cardView;
    }
}