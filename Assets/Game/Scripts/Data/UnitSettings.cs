﻿using UnityEngine;
using Game.Scripts.Base;
using Game.Scripts.Types;
using Game.Scripts.Enums;

namespace Game.Scripts.Data
{
    [System.Serializable]
    [CreateAssetMenu(fileName = "[Unit Data] - ", menuName = "Unit Data")]
    public class UnitSettings : CCGLocalGameData
    {
        [SerializeField]
        [Header("Base data")]
        [Range(0, 100)] private int initiate;
        [SerializeField] [Range(0, 100)] private int health;
        [SerializeField] [Range(0, 100)] private int lightCost;

        [SerializeField] private UnitType unitType;

        //Stats
        [SerializeField]
        [Header("Stats")]
        [Range(0, 500)] private int physicalPower; 
        [SerializeField]
        [Range(0, 500)] private int magicalPower;
        [SerializeField]
        [Range(0, 500)] private int healPower;

        //Resists
        [SerializeField]
        [Header("Resists")]
        [Range(0, 100)] private int physicalResist;
        [SerializeField]
        [Range(0, 100)] private int fireResist;
        [SerializeField]
        [Range(0, 100)] private int poisonResist;


        [SerializeField]
        [Header("Base Light")] private CCGLightType baseLightType;
        [SerializeField]
        [Range(0, 100)] private int baseLight;


        [SerializeField]
        [Header("Extra Light")] private CCGLightType extraLightType;
        [SerializeField]
        [Range(0, 100)] private int extraLight;

        [SerializeField]
        [Header("Description")]
        private string unitName;

        [SerializeField]
        [TextArea(3, 10)] private string description;

        public int Initiate => initiate;

        public string UnitName => unitName;
        public string Description => description;
        public UnitType UnitType => unitType;
        public int Health => health;
        public int LightCost => lightCost;
        
        //Stats
        public int PhysicalPower => physicalPower;
        public int MagicalPower => magicalPower;
        public int HealPower => healPower;

        //Resists
        public int PhysicalResist => physicalResist;
        public int FireResist => fireResist;
        public int PoisonResist => poisonResist;

        public CCGLightType BaseLightType => baseLightType;
        public int BaseLight => baseLight;

        public CCGLightType ExtraLightType => extraLightType;
        public int ExtraLight => extraLight;

    }
}