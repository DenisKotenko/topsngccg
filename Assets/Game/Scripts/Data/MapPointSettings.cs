﻿using Game.Scripts.Base;
using System;
using UnityEngine;
using Game.Scripts.Enums;
using Sirenix.OdinInspector;
using System.Collections.Generic;

namespace Game.Scripts.Data
{
    [Serializable]
    [CreateAssetMenu(fileName = "[Local Data] - Map Point", menuName = "Local Data/Map Point Data")]
      
    public sealed class MapPointSettings : CCGLocalGameData
    {
        [EnumToggleButtons]
        [SerializeField] private MapPointType mapPointType;
        public MapPointType MapPointType => mapPointType;

        [ShowIf("MapPointType", MapPointType.Battle)]
        [SerializeField] private BattleSettings battleSettings;
        public BattleSettings BattleSettings => battleSettings;

        [ShowIf("MapPointType", MapPointType.Town)]
        [SerializeField] private TownSettings townSettings;
        public TownSettings TownSettings => townSettings;

        [ShowIf("MapPointType", MapPointType.Rest)]
        [SerializeField] private RestSettings restSettings;
        public RestSettings RestSettings => restSettings;

        [ShowIf("MapPointType", MapPointType.Event)]
        [SerializeField] private EventSettings eventSettings;
        public EventSettings EventSettings => eventSettings;

        public bool IsPassed { get => isPassed; set => isPassed = value; }

        [Header("Connection settings")]
        [SerializeField] private List<MapPointSettings> listOfPreviousPoints;
        public List<MapPointSettings> ListOfPreviousPoints => listOfPreviousPoints;

        private bool isPassed;

        [Header("Reward settings")]
        [SerializeField] private List<RewardSettings> pointRewards;
        public List<RewardSettings> PointRewards => pointRewards;

    }
}

