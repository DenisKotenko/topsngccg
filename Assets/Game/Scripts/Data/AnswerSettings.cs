﻿using Game.Scripts.Base;
using Sirenix.OdinInspector;
using System;
using UnityEngine;

namespace Game.Scripts.Data
{
    [Serializable]
    [CreateAssetMenu(fileName = "[Local Data] - Answer", menuName = "Local Data/Answer")]
    public class AnswerSettings : CCGLocalGameData
    {
        [SerializeField] private string textOfAnswer;
        [SerializeField] private bool finalAnswer;

        [ShowIf("finalAnswer", true)]
        [SerializeField] private RewardSettings rewardSettings;

        public string TextOfAnswer => textOfAnswer;
        public bool FinalAnswer => finalAnswer;

        public RewardSettings RewardSettings => rewardSettings;

    }
}
