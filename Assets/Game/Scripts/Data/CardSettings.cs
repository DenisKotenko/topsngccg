﻿using System.Collections.Generic;
using UnityEngine;
using Game.Scripts.Enums;
using Game.Scripts.Base;
using Game.Scripts.Data.Actions;

namespace Game.Scripts.Data
{
    [System.Serializable]
    [CreateAssetMenu(fileName = "[Card Data] - ", menuName = "Card Data")]
    public class CardSettings : CCGLocalGameData
    {
        [SerializeField] [Header("Base Light")]
        private CCGLightType baseLightType;

        [SerializeField] private int baseLightCost;

        [SerializeField] [Header("Extra Light")]
        private CCGLightType extraLightType;

        [SerializeField] private int extraLightCost;

        [SerializeField] [Header("Description")]
        private string cardName;
        
        [SerializeField] [TextArea(3, 10)] private string description;
        [Header("Actions")]
        [SerializeField] private List<CardActionSettings> actions = new List<CardActionSettings>();

        public CCGLightType BaseLightType => baseLightType;
        public int BaseLightCost => baseLightCost;

        public CCGLightType ExtraLightType => extraLightType;
        public int ExtraLightCost => extraLightCost;

        public string CardName => cardName;
        public string Description => description;

        public List<CardActionSettings> Actions => actions;

    }
}