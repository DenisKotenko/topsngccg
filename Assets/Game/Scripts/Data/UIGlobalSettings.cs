﻿using Game.Scripts.Base;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
namespace Game.Scripts.Data
{

    [Serializable]
    [CreateAssetMenu(fileName = "[Global Data] - UIGlobalSettings", menuName = "Global Data/UI Data")]
    public class UIGlobalSettings : CCGGlobalGameSettings
    {
        
    }
}