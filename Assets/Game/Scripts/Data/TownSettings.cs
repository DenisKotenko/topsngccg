using System;
using Game.Scripts.Base;
using UnityEngine;

namespace Game.Scripts.Data
{
    [Serializable]
    [CreateAssetMenu(fileName = "[Local Data] - Town", menuName = "Local Data/Town Data")]
    public sealed class TownSettings : CCGLocalGameData
    {
        [SerializeField] private string info;        
        public string Info => info;
        
    }
}