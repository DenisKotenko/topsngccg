﻿using System;
using Game.Scripts.Base;
using Game.Scripts.Controllers.BattleController;
using Game.Scripts.Enums;
using Game.Scripts.TMP;
using Game.Scripts.Types;
using UnityEngine;

namespace Game.Scripts.Data.Actions
{
    [Serializable]
    [CreateAssetMenu(fileName = "[CA] - Card Action",
        menuName = "Local Data/Card Actions/Universal action")]
    public class UniversalAction : CardActionSettings
    {
        [SerializeField]
        private float multiplier;

        [SerializeField] public AttackType attackType;
        [SerializeField] public ActionApplying actionApplying;
        [SerializeField] public AttackSelection attackSelection;
        [SerializeField] public DamageType damageType;
        [SerializeField] public bool isStun;
        [SerializeField] public int roundsCounter;
        [HideInInspector] public AttackDirection attackDirection;

        public override float Multiplier => multiplier;
        public override bool IsStun => isStun;
        public override int RoundsCounter => roundsCounter;
        public override AttackType AttackType => attackType;
        public override DamageType DamageType => damageType;
        public override ActionApplying ActionApplying => actionApplying;
        public override AttackSelection AttackSelection => attackSelection;
        public override AttackDirection AttackDirection => attackDirection;



        public override void Init()
        {
        }

        public override bool Execute(Unit target)
        {
            return UnitParametersSwitcher.ExecuteAction(this, target);
        }
    }
}