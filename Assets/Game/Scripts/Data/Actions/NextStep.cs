﻿using Adic;
using Game.Scripts.Base;
using Game.Scripts.Controllers.BattleController;
using System;
using UnityEngine;

namespace Game.Scripts.Data.Actions
{
    [Serializable]
    [CreateAssetMenu(fileName = "[A] - Step",
        menuName = "Local Data/Actions/Step")]

    public class NextStep : ActionSettings
    {
        [Inject] private IBattleCommand battleCommand;

        public override void Execute()
        {
            battleCommand.NextBattleStep();
        }

        public override void Init()
        {
            if (battleCommand == null)
                battleCommand = GameContextRoot.Container.Resolve<IBattleCommand>();
        }
    }
}