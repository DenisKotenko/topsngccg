﻿using UnityEngine;
using System;
using Adic;
using Game.Scripts.Base;
using Game.Scripts.Controllers.GameStateController;
using Game.Scripts.Enums;

namespace Game.Scripts.Data.Actions
{
    [Serializable]
    [CreateAssetMenu(fileName = "LoadGlobalMap", menuName = "Local Data/Actions/LoadGlobalMap")]

    public class LoadGlobalMap : ActionSettings
    {
        [Inject] private IGameStateCommand iGameStateCommand;

        public override void Execute()
        {
            iGameStateCommand.ChangeGameState(GameState.GlobalMap);
        }

        public override void Init()
        {
            if (iGameStateCommand == null)
                iGameStateCommand = GameContextRoot.Container.Resolve<IGameStateCommand>();
        }
    }
}
