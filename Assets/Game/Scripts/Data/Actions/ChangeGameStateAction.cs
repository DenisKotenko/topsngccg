﻿using System;
using UnityEngine;
using Game.Scripts.Base;
using Game.Scripts.Controllers.GameStateController;
using Game.Scripts.Enums;

namespace Game.Scripts.Data.Actions
{
    [Serializable]
    [CreateAssetMenu(fileName = "[A] - Change Game State",
        menuName = "Local Data/Actions/Change Scene")]
    public class ChangeGameStateAction : ActionSettings
    {
        [SerializeField] GameState newState;

        private IGameStateCommand stateCommand;

        public override void Execute()
        {
            stateCommand.ChangeGameState(newState);
        }

        public override void Init()
        {
            if (stateCommand == null)
                stateCommand = GameContextRoot.Container.Resolve<IGameStateCommand>();
        }
    }
}