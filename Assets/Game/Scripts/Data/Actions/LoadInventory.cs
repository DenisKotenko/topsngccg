﻿using UnityEngine;
using System;
using Game.Scripts.Controllers.PlayersController;
using Adic;
using Game.Scripts.Base;
using Game.Scripts.Controllers.GameStateController;
using Game.Scripts.Enums;
using Game.Scripts.Controllers.InputController;

namespace Game.Scripts.Data.Actions
{
    [Serializable]
    [CreateAssetMenu(fileName = "LoadInventory",
        menuName = "Local Data/Actions/LoadInventory")]

    public class LoadInventory : ActionSettings
    {
        [Inject] private IGameStateCommand iGameStateCommand;
        [Inject] private IGameStateData iGameStateData;
        [Inject] private IInputCommand inputCommand;

        public override void Execute()
        {
            if (iGameStateData.CurrentGameState == GameState.GlobalMap)
            {
                iGameStateCommand.ChangeGameState(GameState.Inventory);
            }
            else if (iGameStateData.CurrentGameState == GameState.Inventory)
            {
                iGameStateCommand.ChangeGameState(GameState.GlobalMap);
            }
            inputCommand.ClearSelected();
        }

        public override void Init()
        {
            if (iGameStateCommand == null)
                iGameStateCommand = GameContextRoot.Container.Resolve<IGameStateCommand>();
            if (iGameStateData == null)
                iGameStateData = GameContextRoot.Container.Resolve<IGameStateData>();
            if (inputCommand == null)
                inputCommand = GameContextRoot.Container.Resolve<IInputCommand>();
        }
    }
}
