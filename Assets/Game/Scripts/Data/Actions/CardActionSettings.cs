using Game.Scripts.Base;
using Game.Scripts.Enums;
using Game.Scripts.Types;

namespace Game.Scripts.Data.Actions
{
    public abstract class CardActionSettings : CCGLocalGameData
    {
        public virtual float Multiplier { get; }
        public virtual bool IsStun { get; }
        /// <summary>
        /// Rounds counter for disable unit
        /// </summary>
        public virtual int RoundsCounter { get; }
        /// <summary>
        /// Attack type (Physical,Magic,etc.)
        /// </summary>
        public virtual AttackType AttackType { get; }
        /// <summary>
        /// Type of damage from attack (Fire,Posion,Physical,etc.)
        /// </summary>
        public virtual DamageType DamageType { get; }
        /// <summary>
        /// Targets for action
        /// </summary>
        public virtual ActionApplying ActionApplying { get; }
        /// <summary>
        /// Single or Massive attack
        /// </summary>
        public virtual AttackSelection AttackSelection { get; }
        /// <summary>
        /// Dirrection for Unit Attack (Column,Row, etc)
        /// </summary>
        public virtual AttackDirection AttackDirection { get; }
        public abstract void Init();
        public abstract bool Execute(Unit target);

    }
}