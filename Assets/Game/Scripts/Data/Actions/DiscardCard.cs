﻿using Adic;
using Game.Scripts.Base;
using Game.Scripts.Controllers.BattleController;
using Game.Scripts.Controllers.InputController;
using System;
using Game.Scripts.Types;
using UnityEngine;

namespace Game.Scripts.Data.Actions
{
    [Serializable]
    [CreateAssetMenu(fileName = "[A] - DiscardCard",
        menuName = "Local Data/Actions/DiscardCard")]
    public class DiscardCard : ActionSettings
    {
        [Inject] private IBattleCommand battleCommand;
        [Inject] private IInputData inputData;

        public override void Execute()
        {
            if (inputData.SelectedObject is Card card)
                battleCommand.DiscardCard(card);
        }

        public override void Init()
        {
            if (battleCommand == null)
                battleCommand = GameContextRoot.Container.Resolve<IBattleCommand>();

            if (inputData == null)
                inputData = GameContextRoot.Container.Resolve<InputController>();
        }
    }
}