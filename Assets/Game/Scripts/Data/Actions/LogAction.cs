﻿using System;
using UnityEngine;

namespace Game.Scripts.Data.Actions
{
    [Serializable]
    [CreateAssetMenu(fileName = "[A] - Log",
        menuName = "Local Data/Actions/Log")]
    public class LogAction : ActionSettings
    {
        [SerializeField] private string message;

        public override void Execute()
        {
            Debug.Log(message);
        }

        public override void Init()
        {
        }
    }
}