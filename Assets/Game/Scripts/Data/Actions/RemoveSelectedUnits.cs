﻿using UnityEngine;
using System;
using Game.Scripts.Controllers.PlayersController;
using Adic;
using Game.Scripts.Base;

namespace Game.Scripts.Data.Actions
{
    [Serializable]
    [CreateAssetMenu(fileName = "RemoveSelectedUnit",
        menuName = "Local Data/Actions/RemoveSelectedUnit")]

    public class RemoveSelectedUnits : ActionSettings
    {
        [Inject] private IPlayerCommand playerCommand;

        public override void Execute()
        {
            playerCommand.ButtonClick();
        }

        public override void Init()
        {
            if (playerCommand == null)
                playerCommand = GameContextRoot.Container.Resolve<PlayerController>();
        }
    }


}