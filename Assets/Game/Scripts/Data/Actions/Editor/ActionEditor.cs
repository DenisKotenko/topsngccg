﻿using System;
using System.Runtime.CompilerServices;
using Game.Scripts.Enums;
using Game.Scripts.TMP;
using UnityEditor;
using UnityEngine;

namespace Game.Scripts.Data.Actions.Editor
{
    [CustomEditor(typeof(UniversalAction))]
    [CanEditMultipleObjects]
    public class ActionEditor : UnityEditor.Editor
    {
        UniversalAction cardAction;
        SerializedProperty Multiplier;
        SerializedProperty AttackType;
        SerializedProperty DamageType;
        SerializedProperty ActionApplying;
        SerializedProperty AttackSelection;
        SerializedProperty AttackDirection;
        SerializedProperty IsStun;
        SerializedProperty RoundsCounter;

        void OnEnable()
        {
            cardAction = target as UniversalAction;
            Multiplier = serializedObject.FindProperty("multiplier");
            IsStun = serializedObject.FindProperty("isStun");
            RoundsCounter = serializedObject.FindProperty("roundsCounter");
            AttackType = serializedObject.FindProperty("attackType");
            DamageType = serializedObject.FindProperty("damageType");
            ActionApplying = serializedObject.FindProperty("actionApplying");
            AttackSelection = serializedObject.FindProperty("attackSelection");
            AttackDirection = serializedObject.FindProperty("attackDirection");
        }


        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.PropertyField(Multiplier);
            EditorGUILayout.Slider(Multiplier, 0, 10, new GUIContent(""));
            EditorGUILayout.Space();
            EditorGUILayout.PropertyField(IsStun);
            if (cardAction.IsStun)
            {
                EditorGUILayout.BeginVertical();
                EditorGUILayout.IntSlider(RoundsCounter, 0, 10, new GUIContent(""));
                EditorGUILayout.EndVertical();
            }
            EditorGUILayout.PropertyField(AttackType);
            if (cardAction.AttackType == Enums.AttackType.Magic)
            {
                EditorGUILayout.BeginFadeGroup(1);
                EditorGUILayout.PropertyField(DamageType);
                EditorGUILayout.Separator();

                EditorGUILayout.EndFadeGroup();

            }

            EditorGUILayout.PropertyField(ActionApplying);
            EditorGUILayout.PropertyField(AttackSelection);
            EditorGUILayout.PropertyField(AttackDirection);


            serializedObject.ApplyModifiedProperties();
        }



    }
}