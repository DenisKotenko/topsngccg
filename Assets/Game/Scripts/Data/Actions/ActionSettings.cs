﻿using System;
using Game.Scripts.Base;

namespace Game.Scripts.Data.Actions
{
    
    [Serializable]
    public abstract class ActionSettings : CCGLocalGameData
    {
        public abstract void Init();
        public abstract void Execute();
    }
}
