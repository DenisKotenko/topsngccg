using System;
using System.Collections.Generic;
using Game.Scripts.Base;
using UnityEngine;

namespace Game.Scripts.Data
{
    [Serializable]
    [CreateAssetMenu(fileName = "[Local Data] - Event", menuName = "Local Data/Event Data")]
    public sealed class EventSettings : CCGLocalGameData
    {
        [SerializeField] private string info;
        public string Info => info;

        [SerializeField]
        [Header("��������� ��������")]
        private List<QuestionStruct> questions;
        [SerializeField]
        [Header("��������� �������")]
        private List<AnswerStruct> answers;

        public List<QuestionStruct> Questions => questions;
        public List<AnswerStruct> Answers => answers;
    }

    [Serializable]
    public struct QuestionStruct
    {
        [SerializeField]
        [Header("������, ������ �������� �������")]
        private List<AnswerSettings> answers;
        [SerializeField]
        [Header("������")]
        private QuestionSettings question;

        public List<AnswerSettings> Answers => answers;
        public QuestionSettings Question => question;
    }

    [Serializable]
    public struct AnswerStruct
    {
        [SerializeField]
        [Header("������ ��� ������")]
        QuestionSettings question;
        [SerializeField]
        [Header("�����")]
        AnswerSettings answer;

        public AnswerSettings Answer => answer;
        public QuestionSettings Question => question;
    }
}