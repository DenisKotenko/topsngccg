using System;
using System.Collections.Generic;
using Game.Scripts.Base;
using Game.Scripts.Types;
using UnityEngine;

namespace Game.Scripts.Data
{
    [Serializable]
    [CreateAssetMenu(fileName = "[Local Data] - Battle", menuName = "Local Data/Battle Data")]
    public sealed class BattleSettings : CCGLocalGameData
    {
        [SerializeField] private string info;
        [SerializeField] private List<Unit> enemies;


        public string Info => info;
        public List<Unit> Enemies => enemies;
    }
}