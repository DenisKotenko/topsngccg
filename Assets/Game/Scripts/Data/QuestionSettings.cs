﻿using Game.Scripts.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Game.Scripts.Data
{
    [Serializable]
    [CreateAssetMenu(fileName = "[Local Data] - Question", menuName = "Local Data/Question")]
    public class QuestionSettings : CCGLocalGameData
    {
        [SerializeField]
        private string textOfQuestion;
        public string TextOfQuestion => textOfQuestion;
    }
}
