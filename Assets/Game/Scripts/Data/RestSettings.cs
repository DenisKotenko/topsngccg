using System;
using Game.Scripts.Base;
using UnityEngine;

namespace Game.Scripts.Data
{
    [Serializable]
    [CreateAssetMenu(fileName = "[Local Data] - Rest", menuName = "Local Data/Rest Data")]
    public sealed class RestSettings : CCGLocalGameData
    {
        [SerializeField] private string info;
        public string Info => info;
    }
}