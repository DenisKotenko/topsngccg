﻿using System;
using System.Collections.Generic;
using Game.Scripts.Base;
using UnityEngine;

namespace Game.Scripts.Data
{
    [Serializable]
    [CreateAssetMenu(fileName = "[Local Data] - Reward", menuName = "Local Data/Reward Data")]

    public sealed class RewardSettings : CCGLocalGameData
    {
        [Header("Light")]
        [SerializeField] private int lightCount;
        public int LightCount => lightCount;

        [Header("Money")]
        [SerializeField] private int moneyCount;
        public int MoneyCount => moneyCount;

        [Header("Cards")]
        [SerializeField] private List<CardSettings> listOfCards;
        public List<CardSettings> ListOfCards => listOfCards;

        [Header("Units")]
        [SerializeField] private List<UnitSettings> listOfUnits;
        public List<UnitSettings> ListOfUnits => listOfUnits;

    }
}
