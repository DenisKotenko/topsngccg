﻿using Adic;
using Game.Scripts.Base;
using Game.Scripts.Controllers.EventController;
using UnityEngine;
using UnityEngine.UI;
using Game.Scripts.Data;
using System.Collections.Generic;

namespace Game.Scripts.Views
{
    public sealed class EventAnswersDeckView : CCGView
    {
        [SerializeField] private Text questionField;
        [SerializeField] private Transform answersField;
        [SerializeField] private AnswerView answerView;

        [Inject] private IEventData eventData;
        [Inject] private IEventListener eventListener;

        [Inject]
        protected override void Init()
        {
            eventListener.OnQuestionLoad += OnQuestionLoad;
        }

        private void OnQuestionLoad()
        {
            questionField.text = eventData.CurrentQuestion.TextOfQuestion;
            List<AnswerStruct> answers = eventData.CurrentEvent.Answers.FindAll(a => { return a.Question == eventData.CurrentQuestion; });
            foreach (AnswerStruct answer in answers)
            {
                AnswerView view = Instantiate(answerView, answersField);
                view.InitField(answer);
            }
        }
        private void OnDestroy()
        {
            eventListener.OnQuestionLoad -= OnQuestionLoad;
        }

    }
}
