﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Game.Scripts.Base;
using Game.Scripts.Data.Actions;
using UnityEngine.EventSystems;


namespace Game.Scripts.Views
{
    public class ButtonView : CCGView, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] private List<ActionSettings> clickActions = new List<ActionSettings>();
        [SerializeField] private List<ActionSettings> pointerEnterActions = new List<ActionSettings>();
        [SerializeField] private List<ActionSettings> pointerExitActions = new List<ActionSettings>();

        public event Action OnClick = () => {};
        public event Action onPointerEnter = () => {};
        public event Action onPointerExit = () => {};

        protected override void Init()
        {
            foreach (var action in clickActions)
            {
                action.Init();
            }
            foreach (var action in pointerEnterActions)
            {
                action.Init();
            }
            foreach (var action in pointerExitActions)
            {
                action.Init();
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            OnClick.Invoke();

            foreach (var action in clickActions)
            {
                if (action != null)
                {
                    action.Execute();
                }
            }
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            onPointerEnter.Invoke();

            foreach (var action in pointerEnterActions)
            {
                if (action != null)
                {
                    action.Execute();
                }
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            onPointerExit.Invoke();

            foreach (var action in pointerExitActions)
            {
                if (action != null)
                {
                    action.Execute();
                }
            }
        }
    }
}
