﻿using Adic;
using Game.Scripts.Base;
using Game.Scripts.Controllers.EventController;
using Game.Scripts.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class AnswerView : CCGView, IPointerClickHandler
{
    [Inject] private IEventCommand eventCommand;
    [Inject] private IEventListener eventListener;
    [Inject] private IEventData eventData;

    [SerializeField]
    private Text textField;
    private AnswerStruct answer;

    protected override void Init()
    {
        eventListener.OnQuestionLoad += OnQuestionLoad;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        print(eventData.pointerPress);
        eventCommand.SelectAnswer(answer.Answer);
        eventCommand.LoadNextQuestion();
    }

    public void InitField(AnswerStruct answerStruct)
    {
        answer = answerStruct;
        textField.text = answer.Answer.TextOfAnswer;
    }

    private void OnQuestionLoad()
    {
        if (answer.Question != eventData.CurrentQuestion)
        {
            Destroy(gameObject);
        }
    }

    protected void OnDestroy()
    {
        eventListener.OnQuestionLoad -= OnQuestionLoad;
    }
}
