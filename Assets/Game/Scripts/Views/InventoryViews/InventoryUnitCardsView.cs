﻿using Adic;
using Game.Scripts.Base;
using Game.Scripts.Controllers.PlayersController;
using Game.Scripts.Controllers.SceneController;
using Game.Scripts.Data;
using Game.Scripts.Types;
using UnityEngine;

namespace Game.Scripts.Views.InventoryViews
{
    public class InventoryUnitCardsView : CCGView
    {
        //[Inject] private ISceneListener sceneListener;
        [Inject] private IPlayerListener playerListener;
        [Inject] private InventorySettings inventorySettings;

        private Card card;

        protected override void Init()
        {
            //sceneListener.OnInventorySceneLoad += FillCardsFromUnit;
            playerListener.OnUnitSelect += FillCardsFromUnit;
        }

        private void FillCardsFromUnit(Unit unit)
        {
            if (this != null)
            {
                foreach (Transform child in transform)
                {
                    Destroy(child.gameObject);
                }
                if (unit != null)
                {
                    foreach (Card card in unit.Deck)
                    {
                        InventoryCardView selectedCard = Instantiate(inventorySettings.CardPrefab, transform);
                        selectedCard.InitCard(card);   
                    }
                }
            }
        }

        private void OnDestroy()
        {
            //sceneListener.OnInventorySceneLoad -= FillCardsFromUnit;
        }
    }
}
