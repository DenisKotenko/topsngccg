﻿using Adic;
using Game.Scripts.Base;
using Game.Scripts.Controllers.InputController;
using Game.Scripts.Controllers.PlayersController;
using Game.Scripts.Controllers.SceneController;
using Game.Scripts.Enums;
using Game.Scripts.Types;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.Views.InventoryViews
{
    public class InventorySelectedUnitView : SelectableView
    {
        [SerializeField] private UnitPosition unitPosition;
        [SerializeField] private Image background;
        [SerializeField] private Text nameSelectedUnit;

        [Inject] private ISceneListener sceneListener;
        [Inject] private IPlayerData playerData;
        [Inject] private IPlayerListener playerListener;        
        [Inject] private IPlayerCommand playerCommand;
        
        private Unit selectedUnit;

        protected override CCGType ObjCCGType => selectedUnit;

        protected override void Init()
        {
            sceneListener.OnInventorySceneLoad += LoadView;
            playerListener.OnInventoryRefresh += LoadView;
            playerListener.OnButtonClick += RemoveSelectedUnit;

        }

        private void LoadView()
        {
            selectedUnit = playerData.SelectedUnits[(int)unitPosition];
            if (selectedUnit == null)
            {
                nameSelectedUnit.text = "Not selected";
                return;
            }
            playerCommand.ChangePosition(selectedUnit, unitPosition);
            nameSelectedUnit.text = selectedUnit.Name;

        }

        protected override void OnClick()
        {
            if (inputData.PreviousSelectedObject is Unit reserveUnit && playerData.ReserveUnits.Contains(reserveUnit))
            {
                if (selectedUnit != null)
                {
                    background.color = Color.white;

                    playerCommand.AddReserveUnitToList(selectedUnit);
                    playerCommand.AddSelectedUnitToList(reserveUnit, (int)unitPosition);
                    playerCommand.RemoveReserveUnitFromList(reserveUnit);

                    inputCommand.ClearSelected();
                }
                else if (selectedUnit == null)
                {
                    playerCommand.AddSelectedUnitToList(reserveUnit, (int)unitPosition);
                    playerCommand.RemoveReserveUnitFromList(reserveUnit);

                    inputCommand.ClearSelected();
                }
            }
        }
        
        private void RemoveSelectedUnit()
        {
            if (inputData.SelectedObject is Unit unit && unit == selectedUnit)
            {
                playerCommand.AddReserveUnitToList(unit);
                playerCommand.RemoveSelectedUnitFromList((int)unitPosition);

                inputCommand.ClearSelected();
            }
        }

        protected override void OnSelected()
        {
            selectedUnit = playerData.SelectedUnits[(int) unitPosition];
            if (selectedUnit == null)
            {
                OnDeselected();                
            }
            else if (background.color == Color.red)
            {
                background.color = Color.white;
                inputCommand.ClearSelected();
                playerCommand.LoadCardsFromDeck(null);
            }
            else
            {
                background.color = Color.red;
                playerCommand.LoadCardsFromDeck(selectedUnit);
            }
        }

        protected override void OnDeselected()
        {
            background.color = Color.white;
        }

        protected override void OnDestroyObject()
        {
            sceneListener.OnInventorySceneLoad -= LoadView;
            playerListener.OnInventoryRefresh -= LoadView;
            playerListener.OnButtonClick -= RemoveSelectedUnit;
        }
    }
}

