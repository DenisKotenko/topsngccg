﻿using Adic;
using Game.Scripts.Base;
using Game.Scripts.Controllers.PlayersController;
using Game.Scripts.Controllers.SceneController;

namespace Game.Scripts.Views.InventoryViews
{
    public class InventoryUnitDeskView:CCGView
    {
        [Inject] private ISceneListener sceneListener;
        [Inject] private IPlayerData playerData;

        
        protected override void Init()
        {
            sceneListener.OnInventorySceneLoad += FillCardsFromUnit;

        }

        private void FillCardsFromUnit()
        {
            
        }

        private void OnDestroy()
        {
            sceneListener.OnInventorySceneLoad -= FillCardsFromUnit;

        }
    }
}
