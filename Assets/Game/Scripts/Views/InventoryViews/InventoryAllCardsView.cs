﻿using Adic;
using Game.Scripts.Base;
using Game.Scripts.Controllers.PlayersController;
using Game.Scripts.Controllers.SceneController;
using Game.Scripts.Data;
using Game.Scripts.Types;

namespace Game.Scripts.Views.InventoryViews
{


    public class InventoryAllCardsView:CCGView
    {
        [Inject] private IPlayerData playerData;
        [Inject] private ISceneListener sceneListener;
        [Inject] private InventorySettings inventorySettings;

        protected override void Init()
        {
            sceneListener.OnInventorySceneLoad += FillCardsList;
        }

        private void FillCardsList()
        {
            if (this != null)
            {
                //foreach (Transform child in transform)
                //{
                //    Destroy(child.gameObject);
                //}

                foreach (Card card in inventorySettings.Cards)
                {
                    InventoryCardView cards = Instantiate(inventorySettings.CardPrefab, transform);
                    cards.InitCard(card);
                }
            }
        }

        private void OnDestroy()
        {
            sceneListener.OnInventorySceneLoad -= FillCardsList;
        }
        
    }
}
