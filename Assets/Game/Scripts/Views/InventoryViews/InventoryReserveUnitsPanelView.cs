﻿using Adic;
using Game.Scripts.Base;
using Game.Scripts.Controllers.PlayersController;
using Game.Scripts.Controllers.SceneController;
using Game.Scripts.Data;
using Game.Scripts.Enums;
using UnityEngine;

namespace Game.Scripts.Views.InventoryViews
{
    public class InventoryReserveUnitsPanelView : CCGView
    {
        [Inject] private IPlayerData playerData;
        [Inject] private IPlayerListener playerListener;
        [Inject] private ISceneListener sceneListener;
        [Inject] private InventorySettings inventorySettings;
        [Inject] private IPlayerCommand playerCommand;


        protected override void Init()
        {
            sceneListener.OnInventorySceneLoad += FillUnitsList;
            playerListener.OnInventoryRefresh += FillUnitsList;
        }

        private void FillUnitsList()
        {
            if (this != null)
            {
                foreach (Transform child in transform)
                {
                    Destroy(child.gameObject);
                }

                for (int i = 0; i < playerData.ReserveUnits.Count; i++)
                {
                    InventoryReserveUnitView reserveComrad = Instantiate(inventorySettings.ReserveUnitPrefab, transform);
                    reserveComrad.InitUnit(playerData.ReserveUnits[i]);
                }
            }
        }

        private void OnDestroyObject()
        {
            sceneListener.OnInventorySceneLoad -= FillUnitsList;
            playerListener.OnInventoryRefresh -= FillUnitsList;
        }
    }
}

