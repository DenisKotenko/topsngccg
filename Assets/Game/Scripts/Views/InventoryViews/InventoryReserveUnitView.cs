﻿using Adic;
using Game.Scripts.Base;
using Game.Scripts.Controllers.InputController;
using Game.Scripts.Controllers.PlayersController;
using Game.Scripts.Enums;
using Game.Scripts.Types;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.Views.InventoryViews
{
    public class InventoryReserveUnitView : SelectableView
    {
        [Inject] private IPlayerCommand playerCommand;
        [Inject] private IPlayerListener playerListener;
        [Inject] private IPlayerData playerData;

        [SerializeField] public Text nameReserveUnit;
        [SerializeField] private Image background;

        private Unit reserveUnit;
        private Color notActiveColor = new Color(0.6452474F, 0.9433962F, 0.7981443F);

        protected override CCGType ObjCCGType => reserveUnit;

        protected override void Init()
        {
        }

        public void InitUnit(Unit unit)
        {
            reserveUnit = unit;
            nameReserveUnit.text = reserveUnit.Name;            
        }
        
        protected override void OnClick()
        {
            if (inputData.PreviousSelectedObject is Unit selectedUnit && playerData.SelectedUnits.Contains(selectedUnit))
            {                
                background.color = notActiveColor;

                playerCommand.AddReserveUnitToList(selectedUnit);
                playerCommand.AddSelectedUnitToList(reserveUnit, (int)selectedUnit.position);
                playerCommand.RemoveReserveUnitFromList(reserveUnit);

                inputCommand.ClearSelected();                
            }       
        }

        protected override void OnSelected()
        {
            if (background.color == Color.red)
            {
                background.color = notActiveColor;
                inputCommand.ClearSelected();
                playerCommand.LoadCardsFromDeck(null);
            }
            else
            {
                background.color = Color.red;
                playerCommand.LoadCardsFromDeck(reserveUnit);
            }
        }

        protected override void OnDeselected()
        {
            background.color = notActiveColor;
        }

        protected override void OnDestroyObject()
        {
        }
    }
}

