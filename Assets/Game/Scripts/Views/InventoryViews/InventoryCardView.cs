﻿using Adic;
using Game.Scripts.Base;
using Game.Scripts.Controllers.PlayersController;
using Game.Scripts.Data;
using Game.Scripts.Types;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.Views.InventoryViews
{
    public class InventoryCardView : SelectableView
    {
        [Inject] private IPlayerListener playerListener;
        [Inject] private InventorySettings inventorySettings;

        [SerializeField] public Text nameCard;
        [SerializeField] public Text baseLight;
        [SerializeField] private Image background;

        private Card card;

        protected override CCGType ObjCCGType => card;

        protected override void Init()
        {
        }

        public void InitCard(Card card)
        {
            this.card = card;
            nameCard.text = card.Name;
            baseLight.text = Convert.ToString(card.BaseLightCost);
        }

        protected override void OnClick()
        {
            Debug.Log(nameCard.text);
        }

        protected override void OnDestroyObject()
        {
        }
    }
}