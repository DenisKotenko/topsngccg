﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adic;
using Game.Scripts.Base;
using Game.Scripts.Controllers.PlayersController;
using Game.Scripts.Controllers.SceneController;
using Game.Scripts.Types;
using UnityEngine;

namespace Game.Scripts.Views.InventoryViews
{
    public class InventorySelectedUnitsPanelView : CCGView
    {
        [Inject] private IPlayerData playerData;
        [Inject] private ISceneListener sceneListener;   

        protected override void Init()
        {
            sceneListener.OnInventorySceneLoad += LoadSelectedUnits;
        }

        private void LoadSelectedUnits()
        {
            
        }

        private void OnDestroy()
        {
            sceneListener.OnInventorySceneLoad -= LoadSelectedUnits;

        }
    }
}
