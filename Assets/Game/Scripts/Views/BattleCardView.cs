﻿using Adic;
using Game.Scripts.Base;
using Game.Scripts.Controllers.BattleController;
using Game.Scripts.Controllers.InputController;
using Game.Scripts.Types;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.Views
{
    public class BattleCardView : SelectableView
    {
        [SerializeField] private Text cardNameText;
        [SerializeField] private Text descriptionText;
        [SerializeField] private Image backgroundImage;
        [SerializeField] private Image cardImage;
        [SerializeField] private Text baseLight;
        [SerializeField] private Text extraLight;
        [SerializeField] private Image frameImage;
        [SerializeField] private Color selectedColor; 
        [SerializeField] private Color deselectedColor;
        private Card card;
        private bool active;

        [Inject] private IBattleListener battleListener;
        [Inject] private IBattleData battleData;


        public Card Card => card;

        protected override CCGType ObjCCGType => card;

        protected override void Init()
        {
            battleListener.OnTurnStart += StartTurn;
            battleListener.OnCardDiscarded += Discard;
            MakeCardInterractable(battleData.CurrentUnit.ActiveDeck.ContainsCard(card));
        }


        public void InitCard(Card card)
        {
            this.card = card;
            cardNameText.text = card.Name;
            descriptionText.text = card.Description;
            baseLight.text = card.BaseLightCost.ToString();
            extraLight.text = card.ExtraLightCost.ToString();
            MakeCardInterractable(false);
        }


        private void StartTurn(Unit unit)
        {
            MakeCardInterractable(unit.ActiveDeck.ContainsCard(Card));
        }

        private void MakeCardInterractable(bool interractable)
        {
            frameImage.enabled = interractable;
            active = interractable;
        }


        protected override void OnClick()
        {
            if (active)
            {
                print(card.Name + " clicked");
            }
            else
            {
                inputCommand.ChangeSelectedObject(null);
            }
        }

        protected override void OnSelected()
        {
            frameImage.color = selectedColor;
        }

        protected override void OnDeselected()
        {
            frameImage.color = deselectedColor;
        }

        private void Discard(Card card)
        {
            if (this.card == card)
            {
                Destroy(gameObject);
            }
        }

        protected override void OnDestroyObject()
        {
            base.OnDestroyObject();
            battleListener.OnTurnStart -= StartTurn;
            battleListener.OnCardDiscarded -= Discard;
        }

    }
}