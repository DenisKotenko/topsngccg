using Adic;
using Game.Scripts.Base;
using Game.Scripts.Controllers.GameStateController;
using Game.Scripts.Controllers.SceneController;
using Game.Scripts.Enums;

namespace Game.Scripts.Views
{
    public class GameLoaderView:CCGView
    {
        [Inject] private ISceneCommand sceneCommand;
        [Inject] private IGameStateCommand gameCommand;
                
        protected override void Init()
        {
            gameCommand.ChangeGameState(GameState.GlobalMap);
            
        }
    }
}