﻿using UnityEngine;
using UnityEngine.EventSystems;
using Game.Scripts.Data;
using UnityEngine.UI;
using Game.Scripts.Controllers.SceneController;
using Adic;
using Game.Scripts.Controllers.PlayersController;
using Game.Scripts.Structs;
using Game.Scripts.Enums;
using Game.Scripts.Base;
using System;
using Game.Scripts.Controllers.GlobalMapController;
using Game.Scripts.Controllers.GameStateController;

namespace Game.Scripts.Views
{
    public class MapPointView : CCGView, IPointerClickHandler
    {
        #region Injections

        [Inject] private IGlobalMapCommand globalMapCommand;
        [Inject] private IGameStateCommand gameStateCommand;

        #endregion

        [SerializeField] private MapPointSettings mapPointSettings;
        [SerializeField] private Text typeOfPointText;

        public void Awake()
        {
            typeOfPointText.text = mapPointSettings.MapPointType.ToString();            
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (globalMapCommand.CanChangePoint(mapPointSettings))
            {
                globalMapCommand.ChangeCurrentMapPoint(mapPointSettings);
                switch (mapPointSettings.MapPointType)
                {
                    case MapPointType.Battle:
                        LoadBattle();
                        break;
                    case MapPointType.Event:
                        LoadEvent();
                        break;
                    case MapPointType.Rest:
                        LoadRest();
                        break;
                    case MapPointType.Town:
                        LoadTown();
                        break;
                }
            }
            else
            {
                print("No path to point " + mapPointSettings);
            }
        }

        private void LoadBattle()
        {
            gameStateCommand.ChangeGameState(GameState.Battle);
        }

        private void LoadEvent()
        {
            gameStateCommand.ChangeGameState(GameState.Event);
            Debug.Log(mapPointSettings.EventSettings.Info);
        }

        private void LoadRest()
        {
            //gameStateCommand.ChangeGameState(GameState.Rest);
            Debug.Log(mapPointSettings.RestSettings.Info);
        }

        private void LoadTown()
        {
            //gameStateCommand.ChangeGameState(GameState.Town);
            Debug.Log(mapPointSettings.TownSettings.Info);
        }
    }
}
