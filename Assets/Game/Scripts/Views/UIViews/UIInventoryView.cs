using Adic;
using Game.Scripts.Base;
using Game.Scripts.Controllers.UIController;
using Game.Scripts.Enums;
using UnityEngine;

namespace Game.Scripts.Views
{
    [RequireComponent(typeof(Canvas))]
    public sealed class UIInventoryView : CCGView
    {
        private const GameState GAMESTATE = GameState.Inventory;

        [Inject] private IUIListener uiStateListener;
        [SerializeField] private Canvas canvas;

        protected override void Init()
        {
            uiStateListener.OnUIChanged += OnUIChanged;

            if (canvas == null)
                canvas = GetComponent<Canvas>();

            canvas.enabled = false;
        }

        private void OnUIChanged(GameState gameState)
        {
            canvas.enabled = (gameState == GAMESTATE);
        }

        private void OnDestroy()
        {
            uiStateListener.OnUIChanged -= OnUIChanged;
        }
    }
}