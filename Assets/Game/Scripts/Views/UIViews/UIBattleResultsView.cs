﻿using Adic;
using Game.Scripts.Base;
using Game.Scripts.Controllers.GameStateController;
using Game.Scripts.Controllers.GlobalMapController;
using Game.Scripts.Controllers.PlayersController;
using Game.Scripts.Controllers.UIController;
using Game.Scripts.Data;
using Game.Scripts.Enums;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.Views
{
    [RequireComponent(typeof(Canvas))]
    public sealed class UIBattleResultsView : CCGView
    {
        private const GameState GAMESTATE = GameState.BattleResults;

        [Inject] private IUIListener uiStateListener;
        [Inject] private IGlobalMapData globalMapData;
        [Inject] private IPlayerCommand playerCommand;
        [SerializeField] private Canvas canvas;
        [Header("Rewards texts")]
        [SerializeField] private Text rewardsTextField;
        [SerializeField] private string rewardsTextWin;
        [SerializeField] private string rewardsTextLose;

        protected override void Init()
        {
            uiStateListener.OnUIChanged += OnUIChanged;

            if (canvas == null)
                canvas = GetComponent<Canvas>();

            canvas.enabled = false;
        }

        private void OnUIChanged(GameState gameState)
        {
            if (gameState == GAMESTATE)
            {
                canvas.enabled = true;
                if (globalMapData.CurrentMapPoint.IsPassed)
                {
                    rewardsTextField.text = rewardsTextWin;

                    if (globalMapData.CurrentMapPoint.PointRewards.Count > 0)
                    {
                        var reward = globalMapData.CurrentMapPoint.PointRewards[0];
                        playerCommand.GetRewards(reward);
                        string rewardString = "\nПолучены в награду юниты : ";
                        foreach (UnitSettings unit in reward.ListOfUnits)
                        {
                            rewardString += unit.name + "; ";
                        }
                        rewardString += "\nПолучены в награду карты : ";
                        foreach (CardSettings card in reward.ListOfCards)
                        {
                            rewardString += card.name + "; ";
                        }
                        rewardsTextField.text += rewardString;
                    }
                }
                else
                {
                    rewardsTextField.text = rewardsTextLose;
                }
            }
            else
            {
                canvas.enabled = false;
            }
        }

        private void OnDestroy()
        {
            uiStateListener.OnUIChanged -= OnUIChanged;
        }
    }
}