using Adic;
using Game.Scripts.Base;
using Game.Scripts.Controllers.BattleController;
using Game.Scripts.Data;
using Game.Scripts.Types;
using System.Collections.Generic;

namespace Game.Scripts.Views
{
    public class BattleDeckView : CCGView
    {
        [Inject] private IBattleListener battleListener;
        [Inject] private IBattleData battleData;
        [Inject] private GlobalBattlesSettings globalBattleSettings;

        [Inject]
        protected override void Init()
        {
            battleListener.OnComradeCardMovedToActiveDeck += FillCardsInHand;
        }

        private void FillCardsInHand(List<Card> cards)
        {
            foreach (Card card in cards)
            {

                var cardView = Instantiate(globalBattleSettings.CardView, transform);
                
                cardView.InitCard(card);
            }
        }

        private void OnDestroy()
        {
            battleListener.OnComradeCardMovedToActiveDeck -= FillCardsInHand;
        }
    }
}