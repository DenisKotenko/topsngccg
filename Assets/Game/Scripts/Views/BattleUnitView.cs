﻿using Adic;
using Game.Scripts.Base;
using Game.Scripts.Controllers.BattleController;
using Game.Scripts.Enums;
using Game.Scripts.Types;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;


namespace Game.Scripts.Views
{
    public class BattleUnitView : SelectableView
    {
        [BoxGroup("Object Init")] [SerializeField]
        private UnitType unitType;

        [BoxGroup("Object Init")] [SerializeField]
        private Image background;

        [BoxGroup("Object Init")] [SerializeField]
        private Text unitInfo;

        [BoxGroup("Object Init")] [SerializeField]
        private Text cardsInfo;

        [BoxGroup("Design")] [SerializeField] private UnitPosition unitPosition;

        [BoxGroup("Design")] [SerializeField] private Color selectedColor;

        [BoxGroup("Design")] [SerializeField] private Color unselectedColor;

        [Inject] private IBattleData battleData;
        [Inject] private IBattleListener battleListener;
        [Inject] private IBattleCommand battleCommand;

        private Unit unit;

        protected override CCGType ObjCCGType => unit;

        protected override void Init()
        {
            battleListener.OnBattleDataLoaded += LoadViewData;
            battleListener.OnTurnStart += Turn;
            battleListener.OnUnitDataChange += UpdateView;
        }

        void UpdateView(Unit updatedUnit)
        {
            if (updatedUnit != unit) return;
            unitInfo.text = $"{unit?.Name} * {unit?.Health.ToString()}";
        }

        private void Turn(Unit unit)
        {
            if (unit == this.unit)
            {
                print($"[{this.unit.Name}] now active with [{this.unit.Initiate}] initiate points");
                OnSelected();
            }
            else
            {
                OnDeselected();
            }
        }

        private void LoadViewData()
        {
            LoadUnit();
            LoadCards();
        }

        private void LoadUnit()
        {
            var i = (int) unitPosition;
            if (unitType == UnitType.Comrade)
            {
                if (i < battleData.Comrades.Count)
                {
                    unit = battleData.Comrades[i];
                }
            }
            else
            {
                if (i < battleData.Enemies.Count)
                {
                    unit = battleData.Enemies[i];
                }
            }



            unitInfo.text = $"{unit?.Name} * {unit?.Health.ToString()}";

            background.color = unselectedColor;
        }

        private void LoadCards()
        {
            cardsInfo.text = "";
            if (unit is null)
            {
                return;
            }

            foreach (Card card in unit.GameDeck)
            {
                cardsInfo.text += card.Name + "\n";
            }
        }

        protected override void OnDestroyObject()
        {
            base.OnDestroyObject();
            battleListener.OnBattleDataLoaded -= LoadViewData;
            battleListener.OnTurnStart -= Turn;
            battleListener.OnUnitDataChange -= UpdateView;
        }

        protected override void OnClick()
        {
            if (unit == null) return;
            if (unit.IsDead) return;
            print(unit.Name + " clicked");
            if (inputData.PreviousSelectedObject is Card card)
            {
                if(battleData.CurrentUnit.BaseLightCount-card.BaseLightCost<0||battleData.CurrentUnit.ExtraLightCount-card.ExtraLightCost<0)
                {
                    MonoBehaviour.print("*-*-*-*-Not Enough Light!-*-*-*-*");
                    return;
                }
                card.Execute(unit);
                
                inputCommand.ClearSelected();
            }
        }

       
        protected override void OnSelected()
        {
            
            if (unit != null)
            {
                if (unit.IsDead) return;
                background.color = selectedColor;
            }
        }

        protected override void OnDeselected()
        {
            if (unit != null)
            {
                background.color = unselectedColor;
            }
        }
    }
}