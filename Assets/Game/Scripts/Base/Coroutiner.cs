﻿using System.Collections;
using UnityEngine;

namespace Game.Scripts.Base
{
    public class Coroutiner:MonoBehaviour
    {
        public void CoroutineStart(IEnumerator routine)
        {
            StartCoroutine(routine);
        }

        public void CoroutineStop(IEnumerator routine)
        {
            StopCoroutine(routine);
        }

        public void AllCoroutinesStop()
        {
            StopAllCoroutines();
        }
    }
}
