﻿using Adic;
using Adic.Container;
using Game.Scripts.Views;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Game.Scripts.Base
{
    public sealed class GameContextRoot : ContextRoot
    {
        [Required] [SerializeField] private DataBindingsSetup dataBindingsSetup;
        [Required] [SerializeField] private ObjectBindingSetup objectBindingSetup;
        
        private static IInjectionContainer container;
        public static IInjectionContainer Container => container;

        public override void SetupContainers()
        {
            container = this.AddContainer<InjectionContainer>().SetupBindings(objectBindingSetup)
                .SetupBindings(dataBindingsSetup).SetupBindings<GameBindingsSetup>();
        }

        public override void Init()
        {
            var GameLoader = new GameObject() {name = "Game Loader"};
            GameLoader.transform.SetParent(this.transform);
            GameLoader.AddComponent<GameLoaderView>();
        }
    }
}