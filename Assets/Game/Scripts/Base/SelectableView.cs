﻿using UnityEngine;
using System.Collections;
using Game.Scripts.Views;
using Adic;
using Game.Scripts.Controllers.InputController;
using Game.Scripts.Types;
using System;

namespace Game.Scripts.Base
{
    [RequireComponent(typeof(ButtonView))]
    public abstract class SelectableView : CCGView
    {
        [Inject] protected IInputCommand inputCommand;
        [Inject] protected IInputListener inputListener;
        [Inject] protected IInputData inputData;

        protected abstract CCGType ObjCCGType { get; }
        private ButtonView buttonView;

        [Inject]
        protected void Initialize()
        {
            buttonView = gameObject.GetComponent<ButtonView>();
            if (buttonView != null)
            {
                buttonView.OnClick += OnPrivateClick;
            }
            inputListener.OnSelectedObjectChange += OnPrivateSelectedObjectChange;
        }

        #region OnChangeSelectedObject

        private void OnPrivateSelectedObjectChange(CCGType selectedObject)
        {
            
            if (selectedObject == ObjCCGType)
            {
                OnSelected();
            }
            else
            {
                OnDeselected();
            }

            OnSelectedObjectChange(selectedObject);
        }

        protected virtual void OnDeselected()
        {
            
        }

        protected virtual void OnSelected()
        {
            
        }

        protected virtual void OnSelectedObjectChange(CCGType selectedObject)
        {

        }

        #endregion

        #region OnClick

        protected virtual void OnClick()
        {    
            
        }
        private void OnPrivateClick()
        {    
            inputCommand.ChangeSelectedObject(ObjCCGType);
            OnClick();
        }

        #endregion

        private void OnDestroy()
        {
            if (buttonView != null)
            {
                buttonView.OnClick -= OnPrivateClick;
            }
            if (inputListener != null)
            {
                inputListener.OnSelectedObjectChange -= OnPrivateSelectedObjectChange;
            }
            OnDestroyObject();
        }

        protected virtual void OnDestroyObject()
        {

        }
    }
}