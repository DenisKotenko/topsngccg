using System.Collections.Generic;
using Adic;
using Adic.Container;
using Game.Scripts.Controllers;
using Game.Scripts.Controllers.BattleController;
using Game.Scripts.Controllers.PlayersController;
using Game.Scripts.Controllers.SceneController;
using Game.Scripts.Controllers.AIController;
using Sirenix.OdinInspector;
using UnityEngine;
using Game.Scripts.Controllers.UIController;
using Game.Scripts.Controllers.GameStateController;
using Game.Scripts.Controllers.InputController;
using Game.Scripts.Controllers.GlobalMapController;
using Game.Scripts.Controllers.EventController;

namespace Game.Scripts.Base
{
    public sealed class GameBindingsSetup :  IBindingsSetup
    {
        public void SetupBindings(IInjectionContainer container)
        {
            container.
                Bind<IInputCommand>().ToSingleton<InputController>().
                Bind<IInputData>().ToSingleton<InputController>().
                Bind<IInputListener>().ToSingleton<InputController>();
            container.
                Bind<IGameStateCommand>().ToSingleton<GameStateController>().
                Bind<IGameStateData>().ToSingleton<GameStateController>().
                Bind<IGameStateListener>().ToSingleton<GameStateController>();
            container.
                Bind<IGlobalMapCommand>().ToSingleton<GlobalMapController>().
                Bind<IGlobalMapData>().ToSingleton<GlobalMapController>().
                Bind<IGlobalMapListener>().ToSingleton<GlobalMapController>();
           //scenes
            container.
                Bind<ISceneCommand>().ToSingleton<SceneController>().
                Bind<ISceneData>().ToSingleton<SceneController>().
                Bind<ISceneListener>().ToSingleton<SceneController>();
            //Player
            container.
                Bind<IPlayerCommand>().ToSingleton<PlayerController>().
                Bind<IPlayerListener>().ToSingleton<PlayerController>().
                Bind<IPlayerData>().ToSingleton<PlayerController>();
            container.
                Bind<IBattleCommand>().ToSingleton<BattleController>().
                Bind<IBattleListener>().ToSingleton<BattleController>().
                Bind<IBattleData>().ToSingleton<BattleController>();
            container.
                Bind<IAICommand>().ToSingleton<AIController>().
                Bind<IAIListener>().ToSingleton<AIController>().
                Bind<IAIData>().ToSingleton<AIController>();
            container.
                Bind<IUICommand>().ToSingleton<UIController>().
                Bind<IUIData>().ToSingleton<UIController>().
                Bind<IUIListener>().ToSingleton<UIController>();
            container.
                Bind<IEventCommand>().ToSingleton<EventController>().
                Bind<IEventData>().ToSingleton<EventController>().
                Bind<IEventListener>().ToSingleton<EventController>();
        }
    }
}