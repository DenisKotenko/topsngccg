using Adic;
using UnityEngine;

namespace Game.Scripts.Base
{
    public abstract class CCGView : MonoBehaviour
    {
        protected void Start()
        {
            this.Inject();
        }

        [Inject]
        protected virtual void Init()
        {
        }
    }
}