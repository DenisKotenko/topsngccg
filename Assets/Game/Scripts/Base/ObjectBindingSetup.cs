﻿using Adic;
using Adic.Container;
using UnityEngine;

namespace Game.Scripts.Base
{
    public sealed class ObjectBindingSetup:MonoBehaviour,IBindingsSetup
   {
       [SerializeField] private Coroutiner coroutiner;

       public void SetupBindings(IInjectionContainer container)
       {
           container.Bind<Coroutiner>().To(coroutiner);
       }
   }
}
