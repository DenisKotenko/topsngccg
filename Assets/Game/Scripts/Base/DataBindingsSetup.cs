using Adic;
using Adic.Container;
using Game.Scripts.Data;
using UnityEngine;

namespace Game.Scripts.Base
{
    public sealed class DataBindingsSetup:MonoBehaviour,IBindingsSetup
    {
        [SerializeField]
        private ScenesSettings scenesSettings;

        [SerializeField] private InventorySettings inventorySettings;
        [SerializeField] private GlobalBattlesSettings globalBattlesSettings;
        [SerializeField] private UIGlobalSettings uIGlobalSettings;
        
        public void SetupBindings(IInjectionContainer container)
        {
            container.
                Bind<InventorySettings>().To(inventorySettings).
                Bind<ScenesSettings>().To(scenesSettings).
                Bind<GlobalBattlesSettings>().To(globalBattlesSettings).
                Bind<UIGlobalSettings>().To(uIGlobalSettings);

        }
    }
}